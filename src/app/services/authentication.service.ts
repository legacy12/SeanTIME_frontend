import { User } from './../shared/staff.model';
import { MessageService } from './../shared/message.service';
import { AuthResult } from './auth-result.enum';
import { AuthBearer } from './AuthBearer.interface';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router, CanActivate } from '@angular/router';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs/Observable';
import 'rxjs/Rx';
import { StaffService } from '../shared/staff.service';

@Injectable()
export class AuthenticationService {
  private tokeyKey = 'token';

  constructor(private http: HttpClient,
              private staffService: StaffService) { }

  public login$(email: string, password: string) {
    const header = new HttpHeaders().set('Content-Type', 'application/json');
    const body = JSON.stringify({ 'Email': email, 'Password': password });
    const options = { headers: header };

    console.log(environment.apiUrl);

    return this.http.put<AuthBearer>(environment.apiUrl + '/api/TokenAuth/Login', body, options).map(
      res => {
        const result = res;
        if (result.state && result.state === AuthResult.Success && result.data && result.data.accessToken) {
          sessionStorage.setItem(this.tokeyKey, result.data.accessToken);
          this.staffService.storeLoggedInUser(result.data.user); // Adds the current user to session
        }
        return result;
      }
    ).shareReplay().catch(this.handleError);
  }

  public authGet$(url) {
    const header = this.initAuthHeaders();
    const options = {headers: header};
    return this.http.get(url, options).shareReplay().catch(this.handleError);
}

public logout() {
  const token = sessionStorage.getItem(this.tokeyKey);
  if (token != null) {
    sessionStorage.removeItem(this.tokeyKey);
    this.staffService.logoutUser();
  }
}

  public checkLogin(): boolean {
    const token = sessionStorage.getItem(this.tokeyKey);
    return token != null;
  }

  public getUserInfo$() {
    return this.authGet$(environment.apiUrl + '/api/TokenAuth');
  }

  public authPost$(url: string, body: any) {
    const headers = this.initAuthHeaders();
    return this.http.post(environment.apiUrl + url, body, { headers: headers }).shareReplay().catch(this.handleError);
  }

  private getLocalToken(): string {
    return sessionStorage.getItem(this.tokeyKey);
  }

  private initAuthHeaders(): HttpHeaders {
    const token = this.getLocalToken();
    if (token == null) {
      throw 'No token';
    }

    const headers = new HttpHeaders()
      .set('Content-Type', 'application/json')
      .set('Authorization', 'Bearer ' + token);
    return headers;
  }

  private handleError(error: any) {
    const errMsg = (error.message) ? error.message : 'Server error';
    console.error(errMsg);
    return Observable.throw(errMsg);
  }
}

    // constructor(private http: Http, private config: AppConfig) { }

    // login(email: string, password: string) {
    //     return this.http.post(this.config.apiUrl + '/api/TokenAuth/Login', { email: email, password: password })
    //         .map((response: Response) => {
    //             // login successful if there's a jwt token in the response
    //             const user = response.json();
    //             console.log(JSON.stringify(user));
    //             if (user && user.token) {
    //                 // store user details and jwt token in local storage to keep user logged in between page refreshes
    //                 localStorage.setItem('currentUser', JSON.stringify(user));
    //             }
    //         });
    // }

    // logout() {
    //     // remove user from local storage to log user out
    //     localStorage.removeItem('currentUser');
    // }
