import { StaffRole } from './../../shared/staff-role.enum';
import { Response } from '@angular/http';
import { StaffService } from '../../shared/staff.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ClientService } from '../../client.service';
import { Subscription } from 'rxjs/Rx';
import { User } from '../../shared/staff.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff-list',
  templateUrl: './staff-list.component.html',
  styleUrls: ['./staff-list.component.css']
})
export class StaffListComponent implements OnInit {
  staffList: User[];
  subscription: Subscription;
  loading = true;
  StaffRole: typeof StaffRole = StaffRole;

  constructor(private clientService: ClientService,
              private staffService: StaffService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.staffService.getStaffList()
    .then(staffList => this.staffList = staffList)
    .then(loading => this.loading = false);
  }

  onSelectStaff(index: string) {
    this.router.navigate([index], {relativeTo: this.route});
  }

  onNewStaffMember() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
