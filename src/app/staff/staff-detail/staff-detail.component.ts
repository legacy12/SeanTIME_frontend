import { StaffService } from '../../shared/staff.service';
import { MessageService } from '../../shared/message.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ClientService } from '../../client.service';
import { Subscription } from 'rxjs/Rx';
import { User } from '../../shared/staff.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-staff-detail',
  templateUrl: './staff-detail.component.html',
  styleUrls: ['./staff-detail.component.css']
})
export class StaffDetailComponent implements OnInit {
  staff: User;
  id: string;
  subscription: Subscription;

  constructor(private clientService: ClientService,
              private staffService: StaffService,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private router: Router) {

              }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params['id'];
          this.staffService.getStaff(this.id)
          .then(staff => this.staff = staff);

          console.log(this.staff);
        }
      );
  }

  onViewJobDetail() {

  }
}
