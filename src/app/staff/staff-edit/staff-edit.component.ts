import { Response } from '@angular/http';
import { StaffService } from '../../shared/staff.service';
import { Job } from '../../shared/job.model';
import { User } from '../../shared/staff.model';
import { MessageService } from '../../shared/message.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ClientService } from '../../client.service';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-staff-edit',
  templateUrl: './staff-edit.component.html',
  styleUrls: ['./staff-edit.component.css']
})
export class StaffEditComponent implements OnInit {
  editMode = false;
  staffForm: FormGroup;
  user: User;

  constructor(private clientService: ClientService,
              private staffService: StaffService,
              private route: ActivatedRoute,
              private messageService: MessageService,
              private router: Router) { }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.initForm();
        }
      );
  }

  onSubmit() {
    const newStaff = new User(
      this.staffForm.value['name'],
      this.staffForm.value['email'],
      this.staffForm.value['staffRole'],
    );

    if (this.editMode) {

    } else {
      this.staffService.addNewStaff(newStaff)
      .then(cancel => this.onCancel());

    }
    this.onCancel();
  }

  onCancel() {
    this.router.navigate(['staff/']);
  }

  private initForm() {
    let staffName = '';
    let staffEmail = '';

    if (this.editMode) {
      this.staffService.getStaff('erter')
      .then(user => this.user = user);

      staffName = this.user.name;
      staffEmail = this.user.email;
    }

    this.staffForm = new FormGroup({
      'name': new FormControl(staffName, Validators.required),
      'email': new FormControl(staffEmail, Validators.required),
      'staffRole': new FormControl(null, Validators.required),
    });
  }
}

