import { User } from '../../shared/staff.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-staff-item',
  templateUrl: './staff-item.component.html',
  styleUrls: ['./staff-item.component.css']
})
export class StaffItemComponent implements OnInit {
  @Input() staff: User;
  @Input() index: number;

  constructor() { }

  ngOnInit() {
  }

}
