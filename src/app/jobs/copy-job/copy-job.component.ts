import { Address } from './../../shared/address.model';
import { JobState } from './../../shared/jobstate.enum';
import { Milestones } from './../../shared/milestones.model';
import { SupplierService } from './../../shared/supplier.service';
import { JobService } from './../../shared/job.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Job } from './../../shared/job.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Supplier } from '../../shared/supplier.model';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Item } from '../../shared/item.model';
import { ContactService } from '../../shared/contact.service';

@Component({
  selector: 'app-copy-job',
  templateUrl: './copy-job.component.html',
  styleUrls: ['./copy-job.component.css']
})
export class CopyJobComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };
  client: Client;
  job: Job;
  supplier: Supplier;
  staff: User;
  contact: Contact;
  id: string; // client ID
  jobId: string; // job ID
  copyMode = false;
  jobForm: FormGroup;
  staffList: User[];
  contactList: Contact[];
  supplierList: Supplier[];
  deliveryAddress: string;
  items: Item[];
  sellingPrice: number;
  markup: number;
  loading = true;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private supplierService: SupplierService,
    private jobService: JobService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.jobId = params['id'];
        this.id = params['cid'];
      }
      );

    if (this.jobId != null) {
      this.copyMode = true;
    }

    Promise.all([this.staffService.getStaffList()
      .then(staffList => this.staffList = staffList),
    this.contactService.getContactList()
      .then(contactList => this.contactList = contactList),
    this.clientService.getClient(this.id)
      .then(client => this.client = client),
    this.jobService.getJob(this.jobId)
      .then(job => this.job = job),
    this.supplierService.getSupplierList()
      .then(supplierList => this.supplierList = supplierList)])
      .then(init => this.initForm())
      .then(log => console.log(this.supplierList))
      .then(id => this.job.id = null)
      .then(loading => this.loading = false);
  }

  onSubmit() {

    console.log(this.jobForm.value['items']);

    if (this.client.jobs == null) {
      this.client.jobs = new Array<Job>();
      console.log('new jobs array');
    }
    this.contact = this.jobForm.value['contact'];
    this.staff = this.jobForm.value['staff'];
    const displayTime = new Date();
    this.items = this.jobForm.value['items'];
    console.log(this.items);
    console.log(this.staff);
    const milestones = new Milestones(displayTime);
    // While the backend still doesn't like full objects being sent this will have to do
    for (const item of this.items) {
      item.supplierId = item.supplier.id;
      item.supplier = null;
    }
    const newJob = new Job(this.id,
      this.staff.id,
      this.contact.id,
      this.jobForm.value['title'],
      this.jobForm.value['description'],
      new Array<Note>(),
      this.jobForm.value['purchasePrice'],
      this.jobForm.value['sellingPrice'],
      this.markup,
      this.items,
      milestones,
      JobState.Production,
      this.deliveryAddress
    );
    console.log(newJob);
    this.jobService.addNewJob(newJob)
      .then(cancel => this.onCancel());
  }

  getControls() {
    return (<FormArray>this.jobForm.get('items')).controls;
  }

  onCancel() {
    this.router.navigate(['clients/', this.id]);
  }

  onDeleteItem(index: number) {
    (<FormArray>this.jobForm.get('items')).removeAt(index);
  }

  onAddItem() {
    (<FormArray>this.jobForm.get('items')).push(
      new FormGroup({
        'product': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ]),
        'supplier': new FormControl(null)
      })
    );
  }

  private initForm() {
    let clientName;
    let jobTitle;
    let jobContact;
    let jobDescription;
    let jobStaff;
    let purchasePrice;
    let sellingPrice;
    const items = new FormArray([]);
    if (this.copyMode) {
      clientName = this.client.name;
      jobTitle = this.job.title;
      jobContact = this.job.contact;
      jobDescription = this.job.description;
      jobStaff = this.job.user;
      purchasePrice = this.job.purchasePrice;
      sellingPrice = this.job.sellingPrice;
      this.markup = this.job.markup;
      if (this.job['items']) {
        for (const item of this.job.items) {
          items.push(
            new FormGroup({
              'product': new FormControl(item.product, Validators.required),
              'amount': new FormControl(item.amount, [
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*$/)
              ]),
              'supplier': new FormControl(item.supplier)
            })
          );
        }
      }
    }

    this.jobForm = new FormGroup({
      'title': new FormControl(jobTitle, Validators.required),
      'contact': new FormControl(jobContact, Validators.required),
      'description': new FormControl(jobDescription, Validators.required),
      'staff': new FormControl(jobStaff, Validators.required),
      'purchasePrice': new FormControl(purchasePrice, Validators.required),
      'customMarkup': new FormControl(this.markup),
      'sellingPrice': new FormControl(sellingPrice, Validators.required),
      'deliveryAddress': new FormControl(this.deliveryAddress, Validators.required),
      'items': items
    });

    const selectedStaff = this.staffList.find(staff => staff.id === this.job.user.id);
    this.job.user = selectedStaff;
    console.log(this.job.user);
    this.jobForm.controls['staff'].setValue(this.job.user, { onlySelf: true });

    const selectedContact = this.contactList.find(contact => contact.id === this.job.contactId);
    this.job.contact = selectedContact;
    console.log(this.job.contact);
    this.jobForm.controls['contact'].setValue(this.job.contact, { onlySelf: true });

    for (let i = 0; i < this.job.items.length; i++) {
      const selectedSupplier = this.supplierList.find(supplier => supplier.id === this.job.items[i].supplier.id);
      this.job.items[i].supplier = selectedSupplier;
      console.log(selectedSupplier);
      (<FormArray>this.jobForm.controls['items']).at(i).get('supplier').setValue(selectedSupplier);
    }
  }

  onMarkup(markup: number) {
    this.markup = markup;
    const purchasePrice = this.jobForm.get('purchasePrice').value;
    this.sellingPrice = (purchasePrice + (purchasePrice * (markup / 100)));
    this.jobForm.controls['sellingPrice'].setValue(purchasePrice + (purchasePrice * (markup / 100)));
    this.jobForm.controls['customMarkup'].setValue('');
  }

  onCustomMarkup() {
    const purchasePrice = this.jobForm.get('purchasePrice').value;
    this.markup = this.jobForm.get('customMarkup').value;
    this.sellingPrice = (purchasePrice + (purchasePrice * (this.markup / 100)));
    this.jobForm.controls['sellingPrice'].setValue(purchasePrice + (purchasePrice * (this.markup / 100)));
  }

  compareFn(s1: User, s2: User): boolean {
    return s1 && s2 ? s1.id === s2.id : s1 === s2;
  }

  onAddressComplete(address: Address) {
    this.deliveryAddress = address.formattedAddress;
  }
}
