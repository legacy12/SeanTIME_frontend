import { JobService } from './../../shared/job.service';
import { ContactService } from './../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Job } from './../../shared/job.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css']
})
export class JobDetailComponent implements OnInit {
  jobForm: FormGroup;
  jobId: string; // job ID
  client: Client;
  id: string; // client ID
  editMode = false;
  job: Job;
  butDisabled = true;
  loading = true;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private jobService: JobService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }


  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.jobId = params['id'];
        if (this.jobId != null) {
          this.jobService.getJob(this.jobId)
            .then(job => this.job = job)
            .then(client => this.client = this.job.client)
            .then(init => this.initForm())
            .then(loading => this.loading = false)
            .then(clientId => this.id = this.job.client.id);
        } else {
          this.loading = false;
          this.initForm();
        }
      }
      );
  }

  private initForm() {
    let jobTitle = '';
    let jobContact = '';
    let jobDescription = '';
    let jobStaff = '';
    let client = '';
    let purchasePrice = 0;
    let sellingPrice = 0;
    let markup = 0;
    let friendlyId = 0;

    jobTitle = this.job.title;
    jobContact = this.job.contact.name;
    jobDescription = this.job.description;
    jobStaff = this.job.user.name;
    purchasePrice = this.job.purchasePrice;
    sellingPrice = this.job.sellingPrice;
    markup = this.job.markup;
    client = this.job.client.name;
    friendlyId = this.job.friendlyId;

    console.log(this.job);

    this.jobForm = new FormGroup({
      'title': new FormControl(jobTitle, Validators.required),
      'contact': new FormControl(jobContact, Validators.required),
      'description': new FormControl(jobDescription, Validators.required),
      'staff': new FormControl(jobStaff, Validators.required),
      'purchasePrice': new FormControl(purchasePrice, Validators.required),
      'sellingPrice': new FormControl(sellingPrice, Validators.required),
      'client': new FormControl(client, Validators.required),
      'markup': new FormControl(markup, Validators.required),
      'friendlyId': new FormControl(friendlyId, Validators.required),
    });

    this.jobForm.get('staff').disable();
    this.jobForm.get('contact').disable();
    this.jobForm.get('client').disable();
  }

  onSubmit() {

  }

  onCancel() {
    this.router.navigate(['clients/' + this.id]);
  }

  onEdit() {
    this.router.navigate(['jobs/edit/' + this.client.id + '/' + this.job.id]);
  }

  onCopy() {
    this.router.navigate(['jobs/copy/' + this.client.id + '/' + this.job.id]);
  }

  onUpgrade() {
    // TODO This works but need to make it look much nicer
    if (window.confirm('Are you sure you want convert this quote to a job?')) {

      for (let item of this.job.items) {
        item.supplierId = item.supplier.id;
        item.supplier = null;
      }

      this.jobService.upgradeJob(this.job).then
      (cancel => this.onCancel());
    }
}

}
