import { Client } from './../../shared/client.model';
import { Router, ActivatedRoute } from '@angular/router';
import { JobService } from './../../shared/job.service';
import { Subscription } from 'rxjs/Subscription';
import { Job } from './../../shared/job.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
  jobList: Job[];
  client: Client;
  loading = true;
  subscription: Subscription;

  constructor(private jobService: JobService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.jobService.getJobList()
    .then(jobList => this.jobList = jobList)
    .then(loading => this.loading = false)
    .then(log => console.log(this.jobList));
  }

  onSelectJob(index: string) {
    this.router.navigate([index], {relativeTo: this.route});
  }

  onNewJob() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
