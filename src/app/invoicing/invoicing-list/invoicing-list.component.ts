import { InvoicingService } from './../../shared/invoicing.service';
import { Client } from './../../shared/client.model';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Job } from './../../shared/job.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-invoicing-list',
  templateUrl: './invoicing-list.component.html',
  styleUrls: ['./invoicing-list.component.css']
})
export class InvoicingListComponent implements OnInit {
  jobList: Job[];
  client: Client;
  loading = true;
  subscription: Subscription;

  constructor(private invoicingService: InvoicingService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.invoicingService.getInvoicingList()
    .then(jobList => this.jobList = jobList)
    .then(loading => this.loading = false)
    .then(log => console.log(this.jobList));
  }

  onSelectJob(index: string) {
    this.router.navigate([index], {relativeTo: this.route});
  }
}
