import { Note } from './note.model';
import { Job } from './job.model';
import { ContactRole } from './contact-role.enum';

export class Contact {
  public id: string;
  public name: string;
  public phoneNumber: number;
  public notes: Note[];
  public email: string;
  public contactRole: ContactRole;
  public jobs: Job[];
  public isPrimary: Boolean;

  constructor(name: string, phoneNumber: number, notes: Note[], email: string, contactRole: ContactRole, jobs: Job[],
              isPrimary: Boolean) {
    this.name = name;
    this.phoneNumber = phoneNumber;
    this.notes = notes;
    this.contactRole = contactRole;
    this.jobs = jobs;
    this.email = email;
  }
}
