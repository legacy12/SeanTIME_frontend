import { Contact } from './contact.model';
import { Supplier } from './supplier.model';
import { SupplierContactItem } from './supplier-contact-item.model';

export class PreItem {
  public id: string;
  public jobId: string;
  public product: string;
  public amount: number;
  public contact: Contact;
  public supplierContactItems: SupplierContactItem[];
  public description: string;

  constructor(product: string, amount: number, supplierContactItems: SupplierContactItem[], description: string) {
      this.product = product;
      this.amount = amount;
      this.description = description;
      this.supplierContactItems = supplierContactItems;
  }
}
