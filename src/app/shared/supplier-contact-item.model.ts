import { Supplier } from './supplier.model';
import { Contact } from './contact.model';

export class SupplierContactItem {
  public id: string;
  public supplierId: string;
  public contactId: string;
  public supplier: Supplier;
  public contact: Contact;

  constructor(supplier: Supplier, contact: Contact) {
    this.supplier = supplier;
    this.contact = contact;
  }

}
