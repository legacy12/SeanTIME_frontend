import { Job } from './job.model';
import { User } from './staff.model';
import { Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';


@Injectable()
export class InvoicingService {
  private jobList: Job[] = new Array<Job>();

  constructor(private http: Http) { }

      /* ------------------------------------------ API calls ---------------------------- */

      updateJob(job: Job): Promise<Job> {
        console.log(job);
        return this.http.put(environment.apiUrl + '/api/Invoicing/' + job.id, job)
          .toPromise()
          .then(response => response.json() as Job)
          .catch(this.handleError);
      }

      getJob(id: string): Promise<Job> {
        return this.http.get(environment.apiUrl + '/api/Invoicing/' + id)
          .toPromise()
          .then(response => response.json().items[0] as Job)
          .catch(this.handleError);
      }

      getInvoicingList() {
        return this.http.get(environment.apiUrl + '/api/Invoicing/')
          .toPromise()
          .then(response => response.json().items as Job[])
          .catch(this.handleError);
      }

      addNewJob(job: Job) {
        return this.http.post(environment.apiUrl + '/api/Invoicing/', JSON.parse(JSON.stringify(job)))
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
      }

      deleteJob(index: string) {
        console.log(index);
        return this.http.delete(environment.apiUrl + '/api/Invoicing/' + index);
      }

      private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
      }

}
