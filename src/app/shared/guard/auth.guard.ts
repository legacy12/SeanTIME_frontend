import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
  private tokeyKey = 'token';
  constructor(private router: Router) { }

  public canActivate() {
    if (this.checkLogin()) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }

  public checkLogin(): boolean {
    const token = sessionStorage.getItem(this.tokeyKey);
    return token != null;
  }

}
