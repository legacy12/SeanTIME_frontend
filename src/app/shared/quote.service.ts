import { Quote } from './quote.model';
import { Injectable } from '@angular/core';
import { User } from './staff.model';
import { Subject } from 'rxjs/Rx';
import { Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class QuoteService {
  private quoteList: Quote[] = new Array<Quote>();

  constructor(private http: Http) { }

  /* ------------------------------------------ API calls ---------------------------- */

  updateQuote(quote: Quote): Promise<Quote> {
    console.log(quote);
    return this.http.put(environment.apiUrl + '/api/Quote/' + quote.id, quote)
      .toPromise()
      .then(response => response.json() as Quote)
      .catch(this.handleError);
  }

  upgradeQuote(quote: Quote): Promise<Quote> {
    console.log(quote);
    return this.http.put(environment.apiUrl + '/api/Quote/Upgrade/' + quote.id, quote)
      .toPromise()
      .then(response => response.json() as Quote)
      .catch(this.handleError);
  }

  getQuote(id: string): Promise<Quote> {
    return this.http.get(environment.apiUrl + '/api/Quote/' + id)
      .toPromise()
      .then(response => response.json().items[0] as Quote)
      .catch(this.handleError);
  }

  getQuoteList() {
    return this.http.get(environment.apiUrl + '/api/Quote')
      .toPromise()
      .then(response => response.json().items as Quote[])
      .catch(this.handleError);
  }

  addNewQuote(qoute: Quote) {
    return this.http.post(environment.apiUrl + '/api/Quote', JSON.parse(JSON.stringify(qoute)))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  deleteQuote(index: string) {
    console.log(index);
    return this.http.delete(environment.apiUrl + '/api/Quote/' + index);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
