import { ContactRole } from './contact-role.enum';
import { AuthResult } from '../services/auth-result.enum';
import { JobService } from './job.service';
import { Contact } from './contact.model';
import { Supplier } from './supplier.model';
import { StaffService } from './staff.service';
import { SupplierService } from './supplier.service';
import { User } from './staff.model';
import { Job } from './job.model';
import { ClientService } from './../client.service';
import { Client } from '../shared/client.model';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

// https://angular.io/tutorial/toh-pt6

@Injectable()
export class ContactService {
    private contactList: Contact[] = new Array<Contact>();

    constructor(private http: Http) { }

    /* ------------------------------------------ API calls ---------------------------- */

    updateContact(contact: Contact): Promise<Contact> {
      return this.http.put(environment.apiUrl + '/api/Contact/' + contact.id, contact)
        .toPromise()
        .then(response => response.json() as Contact)
        .catch(this.handleError);
    }

    getContact(id: string): Promise<Contact> {
      return this.http.get(environment.apiUrl + '/api/Contact/' + id)
        .toPromise()
        .then(response => response.json() as Contact)
        .catch(this.handleError);
    }

    getContactList() {
      return this.http.get(environment.apiUrl + '/api/Contact')
        .toPromise()
        .then(response => response.json().items as Contact[])
        .catch(this.handleError);
    }

    getUnassignedContactList() {
      return this.http.get(environment.apiUrl + '/api/Contact/Unassigned')
        .toPromise()
        .then(response => response.json().items as Contact[])
        .catch(this.handleError);
    }

    getUnassignedRoledContactList(role: ContactRole) {
      return this.http.get(environment.apiUrl + '/api/Contact/Unassigned/' + role)
        .toPromise()
        .then(response => response.json().items as Contact[])
        .catch(this.handleError);
    }

    getSupplierContactList() {
      return this.http.get(environment.apiUrl + '/api/Contact/Suppliers')
        .toPromise()
        .then(response => response.json().items as Contact[])
        .catch(this.handleError);
    }

    getClientContactList() {
      return this.http.get(environment.apiUrl + '/api/Contact/Clients')
        .toPromise()
        .then(response => response.json().items as Contact[])
        .catch(this.handleError);
    }

    addNewContact(contact: Contact) {
      return this.http.post(environment.apiUrl + '/api/Contact', JSON.parse(JSON.stringify(contact)))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
    }

    deleteContact(index: string) {
      console.log(index);
      return this.http.delete(environment.apiUrl + '/api/Contact/' + index);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
    }
  }
