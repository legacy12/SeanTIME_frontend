import { Http } from '@angular/http';
import { User } from './staff.model';
import { Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';

@Injectable()
export class StaffService {
  private staffList: User[] = new Array<User>();
  private currentUser = 'currentUser';

  constructor(private http: Http) { }

  storeLoggedInUser(staff: User) {
    sessionStorage.setItem(this.currentUser, JSON.stringify(staff));
  }

  getLoggedInUser(): User {
    return JSON.parse(sessionStorage.getItem(this.currentUser));
  }

  logoutUser() {
    sessionStorage.removeItem(this.currentUser);
  }

  updateStaff(staff: User): Promise<User> {
    return this.http.put(environment.apiUrl + '/api/User/' + staff.id, staff)
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  getStaff(id: string): Promise<User> {
    return this.http.get(environment.apiUrl + '/api/User/' + id)
      .toPromise()
      .then(response => response.json() as User)
      .catch(this.handleError);
  }

  getStaffList() {
    return this.http.get(environment.apiUrl + '/api/User')
      .toPromise()
      .then(response => response.json().items as User[])
      .catch(this.handleError);
  }

  addNewStaff(staff: User) {
    console.log(JSON.parse(JSON.stringify(staff)));
    return this.http.post(environment.apiUrl + '/api/User', JSON.parse(JSON.stringify(staff)))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  deleteStaff(index: string) {
    console.log(index);
    return this.http.delete(environment.apiUrl + '/api/User/' + index);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
