import { Address } from './address.model';
import { ContactRole } from './contact-role.enum';
import { AuthResult } from '../services/auth-result.enum';
import { JobService } from './job.service';
import { Contact } from './contact.model';
import { Supplier } from './supplier.model';
import { StaffService } from './staff.service';
import { SupplierService } from './supplier.service';
import { User } from './staff.model';
import { Job } from './job.model';
import { ClientService } from './../client.service';
import { Client } from '../shared/client.model';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { environment } from '../../environments/environment';

@Injectable()
export class AddressService {

    constructor(private http: Http) { }

    /* ------------------------------------------ API calls ---------------------------- */

    getAddressList(id: string) {
      return this.http.get(environment.apiUrl + '/api/Address/' + id)
        .toPromise()
        .then(response => response.json().items as Address[])
        .catch(this.handleError);
    }

    updateAddress(address: Address): Promise<Address> {
      return this.http.put(environment.apiUrl + '/api/Address/' + address.id, address)
        .toPromise()
        .then(response => response.json() as Address)
        .catch(this.handleError);
    }

    addNewAddress(address: Address) {
      return this.http.post(environment.apiUrl + '/api/Address', JSON.parse(JSON.stringify(address)))
      .toPromise()
      .then(response => response.json().items as Address[])
      .catch(this.handleError);
    }

    deleteAddress(index: string) {
      return this.http.delete(environment.apiUrl + '/api/Address/' + index)
      .toPromise()
      .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
      console.error('An error occurred', error); // for demo purposes only
      return Promise.reject(error.message || error);
    }
  }
