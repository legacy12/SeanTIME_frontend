import { JobState } from './jobstate.enum';
import { Milestones } from './milestones.model';
import { Supplier } from './supplier.model';
import { Client } from './client.model';
import { Contact } from './contact.model';
import { User } from './staff.model';
import { Note } from './note.model';
import { Item } from './item.model';

export class Quote {
  public id: string;
  public friendlyId: number;
  public clientId: string;
  public userId: string;
  public supplierId: string;
  public contactId: string;
  public title: string;
  public contact: Contact;
  public client: Client;
  public supplier: Supplier;
  public description: string;
  public user: User;
  public notes: Note[];
  public purchasePrice: number;
  public sellingPrice: number;
  public markup: number;
  public items: Item[];
  public milestones: Milestones;
  public dueDate;
  public jobState: JobState;
  public deliveryAddress: string;

  constructor(clientId: string, userId: string, contactId: string, title: string,
      description: string, notes: Note[], purchasePrice: number, sellingPrice: number, markup: number,
       milestones: Milestones, items: Item[], jobState: JobState, deliveryAddress: string) {
    this.clientId = clientId;
    this.userId = userId;
    this.contactId = contactId;
    this.title = title;
    this.description = description;
    this.notes = notes;
    this.purchasePrice = purchasePrice;
    this.sellingPrice = sellingPrice;
    this.markup = markup;
    this.items = items;
    this.milestones = milestones;
    this.jobState = jobState;
    this.deliveryAddress = deliveryAddress;
  }
}
