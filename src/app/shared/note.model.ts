import { User } from './../shared/staff.model';


export class Note {
  public jobId: string;
  public date: String;
  public staffName: string;  // TODO Change type to staff
  public message: string;

  constructor(jobId: string, date: String, staffName: string, message: string) {
    this.jobId = jobId;
    this.date = date;
    this.staffName = staffName;
    this.message = message;
  }
}
