import { Contact } from './contact.model';
import { Issue } from './issue.model';
import { Address } from './address.model';

export class Supplier {
  public id: string;
  public contacts: Contact[];
  public name: string;
  public phoneNumber: number;
  public issues: Issue[];
  public addresses: Address[];

  constructor(name: string, phone: number, contacts: Contact[], issues: Issue[]) {
    this.name = name;
    this.phoneNumber = phone;
    this.issues = issues;
    this.contacts = contacts;
  }
}
