import { Job } from './job.model';
import { User } from './staff.model';
import { Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { environment } from '../../environments/environment';

@Injectable()
export class JobService {
    private jobList: Job[] = new Array<Job>();

    constructor(private http: Http) { }

        /* ------------------------------------------ API calls ---------------------------- */

        updateJob(job: Job): Promise<Job> {
          console.log(job);
          return this.http.put(environment.apiUrl + '/api/Job/' + job.id, job)
            .toPromise()
            .then(response => response.json() as Job)
            .catch(this.handleError);
        }

        upgradeJob(job: Job): Promise<Job> {
          console.log(job);
          return this.http.put(environment.apiUrl + '/api/Job/Upgrade/' + job.id, job)
            .toPromise()
            .then(response => response.json() as Job)
            .catch(this.handleError);
        }

        getJob(id: string): Promise<Job> {
          return this.http.get(environment.apiUrl + '/api/Job/' + id)
            .toPromise()
            .then(response => response.json().items[0] as Job)
            .catch(this.handleError);
        }

        getJobList() {
          return this.http.get(environment.apiUrl + '/api/Job')
            .toPromise()
            .then(response => response.json().items as Job[])
            .catch(this.handleError);
        }

        addNewJob(job: Job) {
          return this.http.post(environment.apiUrl + '/api/Job', JSON.parse(JSON.stringify(job)))
          .toPromise()
          .then(response => response.json())
          .catch(this.handleError);
        }

        deleteJob(index: string) {
          console.log(index);
          return this.http.delete(environment.apiUrl + '/api/Job/' + index);
        }

        private handleError(error: any): Promise<any> {
          console.error('An error occurred', error); // for demo purposes only
          return Promise.reject(error.message || error);
        }

}
