export class Milestones {
  public id: string;
  public created: Date;
  public started: Date;
  public updated: Date;
  public invoiced: Date;
  public completed: Date;
  public due: Date;

  constructor(created: Date) {
    this.created = created;
  }
}


