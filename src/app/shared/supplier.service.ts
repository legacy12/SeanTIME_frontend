import { environment } from './../../environments/environment';
import { Supplier } from './supplier.model';
import { Subject } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';


@Injectable()
export class SupplierService {
  private supplierList: Supplier[] = new Array<Supplier>();

  constructor(private http: Http) { }

  updateSupplier(supplier: Supplier): Promise<Supplier> {
    return this.http.put(environment.apiUrl + '/api/Supplier/' + supplier.id, supplier)
      .toPromise()
      .then(response => response.json() as Supplier)
      .catch(this.handleError);
  }

  getSupplier(id: string): Promise<Supplier> {
    return this.http.get(environment.apiUrl + '/api/Supplier/' + id)
      .toPromise()
      .then(response => response.json() as Supplier)
      .catch(this.handleError);
  }

  getSupplierList() {
    return this.http.get(environment.apiUrl + '/api/Supplier')
      .toPromise()
      .then(response => response.json().items as Supplier[])
      .catch(this.handleError);
  }

  addNewSupplier(supplier: Supplier) {
    console.log(JSON.parse(JSON.stringify(supplier)));
    return this.http.post(environment.apiUrl + '/api/Supplier', JSON.parse(JSON.stringify(supplier)))
    .toPromise()
    .then(response => response.json())
    .catch(this.handleError);
  }

  deleteSupplier(index: string) {
    console.log(index);
    return this.http.delete(environment.apiUrl + '/api/Supplier/' + index);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
