import { Supplier } from './supplier.model';
export class Item {
  public id: string;
  public jobId: string;
  public product: string;
  public amount: number;
  public supplier: Supplier;
  public supplierId: string;
  public description: string;

  constructor(product: string, amount: number, supplierId: string, description: string) {
      this.product = name;
      this.amount = amount;
      this.supplierId = supplierId;
      this.description = description;
  }
}
