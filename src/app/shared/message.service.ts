import { User } from './staff.model';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class MessageService {

  private clientSource = new BehaviorSubject<string>(null);
  private jobSource = new BehaviorSubject<number>(null);
  private staffSource = new BehaviorSubject<User>(null);

  currentJob = this.jobSource.asObservable();
  currentClient = this.clientSource.asObservable();
  currentStaff = this.staffSource.asObservable();

  constructor() { }

  changeClient(client: string) {
    this.clientSource.next(client);
  }

  changeJob(job: number) {
    this.jobSource.next(job);
  }

  changeStaff(staff: User) {
    console.log('staff in MS: ' + staff);
    this.staffSource.next(staff);
  }



}
