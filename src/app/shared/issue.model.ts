export class Issue {
  created: Date;
  message: string;

  constructor(created: Date, message: string) {
    this.created = created;
    this.message = message;
  }
}
