import { environment } from './../../environments/environment';
import { Http } from '@angular/http';
import { PreProduction } from './pre-production.model';
import { Injectable } from '@angular/core';

@Injectable()
export class PreProductionService {
  private preProductionList: PreProduction[] = new Array<PreProduction>();

  constructor(private http: Http) { }

  /* ------------------------------------------ API calls ---------------------------- */

  updatePreProduction(preProduction: PreProduction): Promise<PreProduction> {
    return this.http.put(environment.apiUrl + '/api/PreProduction/' + preProduction.id, preProduction)
      .toPromise()
      .then(response => response.json() as PreProduction)
      .catch(this.handleError);
  }

  upgradePreProduction(preProduction: PreProduction): Promise<PreProduction> {
    return this.http.put(environment.apiUrl + '/api/PreProduction/Upgrade/' + preProduction.id, preProduction)
      .toPromise()
      .then(response => response.json() as PreProduction)
      .catch(this.handleError);
  }

  getPreProduction(id: string): Promise<PreProduction> {
    return this.http.get(environment.apiUrl + '/api/PreProduction/' + id)
      .toPromise()
      .then(response => response.json().items[0] as PreProduction)
      .catch(this.handleError);
  }

  getPreProductionList() {
    return this.http.get(environment.apiUrl + '/api/PreProduction')
      .toPromise()
      .then(response => response.json().items as PreProduction[])
      .catch(this.handleError);
  }

  addNewPreProduction(preProduction: PreProduction) {
    return this.http.post(environment.apiUrl + '/api/PreProduction', JSON.parse(JSON.stringify(preProduction)))
      .toPromise()
      .then(response => response.json())
      .catch(this.handleError);
  }

  deletePreProduction(index: string) {
    console.log(index);
    return this.http.delete(environment.apiUrl + '/api/PreProduction/' + index);
  }

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error); // for demo purposes only
    return Promise.reject(error.message || error);
  }
}
