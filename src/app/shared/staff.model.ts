import { StaffRole } from './staff-role.enum';
import { Job } from './../shared/job.model';
export class User {
  public id: string;
  public name: string;
  public role: StaffRole;
  public email: string;

    constructor(name: string, email: string, role: StaffRole) {
        this.name = name;
        this.email = email;
        this.role = role;
    }

}
