import { PreItem } from './pre-item.model';
import { JobState } from './jobstate.enum';
import { Milestones } from './milestones.model';
import { Client } from './client.model';
import { Contact } from './contact.model';
import { User } from './staff.model';
import { Note } from './note.model';
import { Item } from './item.model';

export class PreProduction {
  public id: string;
  public friendlyId: number;
  public clientId: string;
  public userId: string;
  public contactId: string;
  public title: string;
  public contact: Contact;
  public client: Client;
  public description: string;
  public user: User;
  public notes: Note[];
  public preItems: PreItem[];
  public milestones: Milestones;
  public jobState: JobState;
  public deliveryAddress: string;

  constructor(clientId: string, userId: string, contactId: string, title: string, description: string, notes: Note[],
    milestones: Milestones, preItems: PreItem[], jobState: JobState, deliveryAddress: string) {
    this.clientId = clientId;
    this.userId = userId;
    this.contactId = contactId;
    this.title = title;
    this.description = description;
    this.notes = notes;
    this.milestones = milestones;
    this.jobState = jobState;
    this.deliveryAddress = deliveryAddress;
    this.preItems = preItems;
  }
}
