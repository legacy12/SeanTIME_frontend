import { FormGroup } from '@angular/forms';
import { PreItem } from './../../pre-item.model';
import { Supplier } from './../../supplier.model';
import { SupplierService } from './../../supplier.service';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'app-pre-item-list',
  templateUrl: './pre-item-list.component.html',
  styleUrls: ['./pre-item-list.component.css']
})
export class PreItemListComponent implements OnInit {
  @Input() preItemList: PreItem[];
  @Input() supplierList: Supplier[];
  @Input() inputForm: FormGroup[];
  @Output() preItemListEvent = new EventEmitter<PreItem[]>();
  @Input() viewMode = false;
  newItem = false;

  constructor(private supplierService: SupplierService) { }

  ngOnInit() {
    console.log(this.inputForm);
  }

  onAddNewItem() {
    this.newItem = true;
  }

  onNewItem(preItem: PreItem) {
    if (this.preItemList == null) {
      this.preItemList = new Array<PreItem>();
    }
    if (preItem != null) {
      this.preItemList.push(preItem);
      this.newItem = false;
      this.onSubmit();
    } else {
      this.newItem = false;
    }
  }

  onSubmit() {
    this.preItemListEvent.emit(this.preItemList);
  }

  onDeleteItem(preItem: PreItem) {
    const index: number = this.preItemList.indexOf(preItem);
    if (index !== -1) {
        this.preItemList.splice(index, 1);
        this.preItemListEvent.emit(this.preItemList);
        console.log('deleted item');
    }
  }
}
