import { FormGroup } from '@angular/forms';
import { Address } from './../../address.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-address-selector',
  templateUrl: './address-selector.component.html',
  styleUrls: ['./address-selector.component.css']
})
export class AddressSelectorComponent implements OnInit {
  @Input() addressList: Address[];
  @Input() inputForm: FormGroup;
  @Output() addressEvent = new EventEmitter<Address>();
  selectedAddress: Address;

  constructor() { }

  ngOnInit() {
  }

  onChange(address) {
    console.log(address);
    this.selectedAddress = address;
    this.addressEvent.emit(address);
  }

}
