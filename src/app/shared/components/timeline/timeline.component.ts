import { PreItem } from './../../pre-item.model';
import { Quote } from './../../quote.model';
import { JobService } from './../../job.service';
import { StaffService } from './../../staff.service';
import { Response } from '@angular/http';
import { Note } from './../../note.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Job } from './../../job.model';
import { MessageService } from '../../message.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ClientService } from '../../../client.service';
import { Subscription } from 'rxjs/Rx';
import { Client } from './../../client.model';
import { Component, OnInit, Input } from '@angular/core';
import { User } from '../../staff.model';
import { QuoteService } from '../../quote.service';
import { PreProduction } from '../../pre-production.model';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.css']
})
export class TimelineComponent implements OnInit {
  collapse = true;
  inverted = false;
  user: User;
  @Input() job: Job;
  @Input() client: Client;
  @Input() quote: Quote;
  @Input() preProd: PreProduction;
  jobId: number;
  id: string;
  subscription: Subscription;
  noteForm: FormGroup;

  constructor(private clientService: ClientService,
    private route: ActivatedRoute,
    private staffService: StaffService,
    private jobService: JobService,
    private quoteService: QuoteService,
    private router: Router) {
  }

  ngOnInit() {
    this.user = this.staffService.getLoggedInUser();
    this.initForm();
  }

  private initForm() {
    const note = '';

    console.log(this.user);

    this.noteForm = new FormGroup({
      'note': new FormControl(note, Validators.required),
    });
  }

  onSubmit() {
    console.log(this.job);
    const displayTime = new Date().toLocaleTimeString();

    if (this.job != null) {

      const newNote = new Note(
        this.job.id,
        displayTime,
        this.user.name,
        this.noteForm.value['note'],
      );

      console.log('New note for a job');
      this.job.notes.push(newNote);
      this.jobService.updateJob(this.job)
        .then(collapse => this.collapse = true);
    }

    if (this.quote != null) {

      const newNote = new Note(
        this.quote.id,
        displayTime,
        this.user.name,
        this.noteForm.value['note'],
      );

      console.log('New note for quote');
      this.quote.notes.push(newNote);
      this.quoteService.updateQuote(this.quote)
        .then(collapse => this.collapse = true);
    }
  }
}
