import { Contact } from './../../contact.model';
import { Supplier } from './../../supplier.model';
import { FormGroup, FormControl, Validators, FormArray } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, Input, ViewChild } from '@angular/core';
import { PreItem } from '../../pre-item.model';
import { SupplierContactItem } from '../../supplier-contact-item.model';

@Component({
  selector: 'app-pre-item',
  templateUrl: './pre-item.component.html',
  styleUrls: ['./pre-item.component.css']
})
export class PreItemComponent implements OnInit {
  @Input()inputForm: FormGroup;
  @Input() supplierList: Supplier[];
  @Output() preItemEvent = new EventEmitter<PreItem>();
  selectedSupplier = false;
  supplier: Supplier;
  contact: Contact;
  selectedSuppliers: SupplierContactItem[];

  constructor() { }

  ngOnInit() {
  }


  onSubmit() {
    const selectedSuppliers = new Array();
    const product = this.inputForm.controls['preItems'].value.product;
    const amount = this.inputForm.controls['preItems'].value.amount;
    const description = this.inputForm.controls['preItems'].value.itemDescription;
    const items: SupplierContactItem[] = this.inputForm.value['items'];
    const preItem = new PreItem(product, amount, items, description);
    this.preItemEvent.emit(preItem);
  }

  onCancel() {
    this.preItemEvent.emit(null);
  }
}
