import { Contact } from './../../contact.model';
import { FormGroup, FormArray, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Supplier } from '../../supplier.model';
import { SupplierContactItem } from '../../supplier-contact-item.model';

@Component({
  selector: 'app-supplier-contact-item',
  templateUrl: './supplier-contact-item.component.html',
  styleUrls: ['./supplier-contact-item.component.css']
})
export class SupplierContactItemComponent implements OnInit {
  selectedSupplier = false;
  @Input() inputForm: FormGroup;
  @Input() supplierList: Supplier[];
  supplier: Supplier;
  contact: Contact;
  selectedSuppliers: SupplierContactItem[];

  constructor() { }

  ngOnInit() {
  }

  getControls() {
    return (<FormArray>this.inputForm.get('items')).controls;
  }

  onDeleteItem(index: number) {
    (<FormArray>this.inputForm.get('items')).removeAt(index);
  }

  onAddItem() {
    (<FormArray>this.inputForm.get('items')).push(
      new FormGroup({
        'supplier': new FormControl(null, Validators.required),
        'contact': new FormControl(null, Validators.required)
      })
    );
  }
}
