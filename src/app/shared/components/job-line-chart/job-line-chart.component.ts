import { Client } from './../../client.model';
import { User } from './../../staff.model';
import { Component, OnInit, Input } from '@angular/core';
import { Job } from '../../job.model';
import { ChartModule } from 'primeng/primeng';

@Component({
  selector: 'app-job-line-chart',
  templateUrl: './job-line-chart.component.html',
  styleUrls: ['./job-line-chart.component.css']
})
export class JobLineChartComponent {

  @Input() client: Client;
  @Input() id: String;

  public data: any = {
    labels: [],
    datasets: []
  };

  constructor() {
    console.log(this.id);
    console.log(this.client);
    for (let i = 0; i < this.client.jobs.length; i++) {
      const orderDate = new Date(this.client.jobs[i].orderDate);
      this.data.datasets.push({
        label: orderDate.getMonth(),
        data: 1,
        fillcolor: '#69D2E7'
      });
    }
  }
}
