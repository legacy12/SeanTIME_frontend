import { AddressService } from './../../address.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Supplier } from './../../supplier.model';
import { ClientService } from './../../../client.service';
import { Address } from './../../address.model';
import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { SupplierService } from '../../supplier.service';
import { Client } from '../../client.model';

@Component({
  selector: 'app-address-book',
  templateUrl: './address-book.component.html',
  styleUrls: ['./address-book.component.css']
})
export class AddressBookComponent implements OnInit {
  @Input() addressList: Address[];
  @Input() ownerId: string;
  inputForm: FormGroup;
  newAddress = false;

  constructor(private route: ActivatedRoute,
    private router: Router,
    private clientService: ClientService,
    private supplierService: SupplierService,
    private addressService: AddressService
    ) { }

  ngOnInit() {
    this.route.params
    .subscribe(
    (params: Params) => {
    }
    );

    this.inputForm = new FormGroup({
      'addressControl': new FormControl(null, Validators.required),
    });
  }

  onDeleteAddress(address: Address) {
    if (window.confirm('Are you sore you want to delete this address?')) {
      this.addressService.deleteAddress(address.id)
      .then(update => this.updateAddresses());
    }
  }

  onComplete() {
  }

  onAddNewAddress() {
    this.newAddress = !this.newAddress;
  }

  onAddressComplete(address: Address) {
    // TODO onAddressComplete gets triggered too early
    if (address != null) {
      address.clientId = this.ownerId;

      this.addressService.addNewAddress(address)
        .then(update => this.updateAddresses());
      this.newAddress = false;
    }
  }

  updateAddresses() {
    this.addressService.getAddressList(this.ownerId)
    .then(addressList => this.addressList = addressList);
  }
}
