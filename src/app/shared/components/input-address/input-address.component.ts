import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Component, OnInit, Output, EventEmitter, NgZone, ViewChild, ElementRef, Input } from '@angular/core';
import { Address } from '../../address.model';
import { MapsAPILoader } from '@agm/core';
import {} from '@types/googlemaps';

@Component({
  selector: 'app-input-address',
  templateUrl: './input-address.component.html',
  styleUrls: ['./input-address.component.css']
})
export class InputAddressComponent implements OnInit {
  @Output() addressEvent = new EventEmitter<Address>();
  @Input() inputForm: FormGroup;

  address: Address;
  editMode: false;

  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone) { }

  ngOnInit() {
    // load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      const autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address']
      });
      console.log(autocomplete);
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          // get the place result
          const place: google.maps.places.PlaceResult = autocomplete.getPlace();

          // verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }

          this.address = new Address();

          this.address.formattedAddress = place.formatted_address;

          for (let i = 0; i < place.address_components.length;  i++) {
            if (place.address_components[i].types[0] === 'street_number') {
              this.address.number = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[0] === 'route') {
              this.address.name = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[0] === 'postal_town') {
              this.address.town = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[0] === 'administrative_area_level_1') {
              this.address.province = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[0] === 'country') {
              this.address.country = place.address_components[i].long_name;
            }
            if (place.address_components[i].types[0] === 'postal_code') {
              this.address.postcode = place.address_components[i].long_name;
            }
          }

          this.address.isPrimary = true;
          this.addressEvent.emit(this.address);

        });
      });
    });
  }
}
