export class Address {
  public id: string;
  public clientId: string;
  public country: string;
  public number: string;
  public postcode: string;
  public province: string;
  public town: string;
  public name: string;
  public isPrimary: Boolean;
  public formattedAddress: string;

  constructor() {}

}
