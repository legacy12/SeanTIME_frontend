import { User } from './staff.model';
import { Address } from './address.model';
import { Quote } from './quote.model';
import { Contact } from './contact.model';
import { Job } from './../shared/job.model';
export class Client {
  public id: string;
  public name: string;
  public contact: Contact;
  public user: User;
  public userId: string;
  public contactId: string;
  public description: string;
  public jobs: Job[];
  public quotes: Quote[];
  public addresses: Address[];
  public onStop: Boolean;

  constructor(name: string, description: string, jobs: Job[], quotes: Quote[], contact: Contact, userId: string, addresses: Address[]) {
    this.name = name;
    this.description = description;
    this.jobs = jobs;
    this.quotes = quotes;
    this.contact = contact;
    this.addresses = addresses;
    this.userId = userId;
  }
}
