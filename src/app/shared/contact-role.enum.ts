export enum ContactRole {
  Lead,
  Client,
  Job,
  Supplier
}
