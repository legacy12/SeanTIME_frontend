    export enum JobState {
        Preproduction,
        AwaitingSuppliers,
        Lost,
        Quote,
        Production,
        Invoicing,
        Completed
    }
