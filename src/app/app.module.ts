import { JobLineChartComponent } from './shared/components/job-line-chart/job-line-chart.component';
import { StaffService } from './shared/staff.service';
import { MessageService } from './shared/message.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationService } from './services/authentication.service';
import { AlertService } from './services/alert.service';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './shared/guard/auth.guard';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Http, HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NotificationsComponent } from './navmenu/notifications/notifications.component';
import { ChartModule } from 'primeng/primeng';

@NgModule({
  declarations: [
      AppComponent,
      LoginComponent,
      NotificationsComponent,
  ],
  imports: [
      BrowserModule,
      BrowserAnimationsModule,
      FormsModule,
      HttpModule,
      AppRoutingModule,
      HttpClientModule,
      ChartModule
  ],
  providers: [AuthGuard, AlertService, StaffService, AuthenticationService, MessageService],
  bootstrap: [AppComponent]
})
export class AppModule { }
