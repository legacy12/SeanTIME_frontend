import { AuthResult } from '../services/auth-result.enum';
import { AlertService } from './../services/alert.service';
import { AuthenticationService } from './../services/authentication.service';
import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  animations: [routerTransition()]
})
export class LoginComponent implements OnDestroy {
  model: any = {};
  private userName: string;
  private password: string;
  private postStream$: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    private alertService: AlertService) { }


  login() {
    if (this.postStream$) {this.postStream$.unsubscribe(); }

    this.postStream$ = this.authenticationService.login$(this.model.email, this.model.password).subscribe(
      result => {
        if (result.state == AuthResult.Success) {
          this.router.navigate(['/dashboard']);
        } else {
          alert(result.msg);
        }
      }
    );
  }

  ngOnDestroy() {
    if (this.postStream$) {this.postStream$.unsubscribe(); }
}
}
