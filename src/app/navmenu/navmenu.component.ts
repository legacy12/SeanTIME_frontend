import { Router } from '@angular/router';
import { AuthenticationService } from './../services/authentication.service';
import { MessageService } from '../shared/message.service';
import { User } from './../shared/staff.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-nav-menu',
  templateUrl: './navmenu.component.html',
  styleUrls: ['./navmenu.component.css']
})
export class NavMenuComponent {
  staff: User;
  collapse = true;
  settingsCollapse = true;

  constructor(private messageService: MessageService,
    private authenticationService: AuthenticationService,
    private router: Router) {
    this.messageService.currentStaff.subscribe(staff => this.staff = staff);
  }

  onLogout() {
    this.authenticationService.logout();
    this.router.navigate(['/login']);
    console.log('logout');
  }
}

