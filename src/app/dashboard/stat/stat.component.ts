import { Router } from '@angular/router';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'app-stat',
    templateUrl: './stat.component.html',
    styleUrls: ['./stat.component.css']
})
export class StatComponent implements OnInit {
    @Input() bgClass: string;
    @Input() icon: string;
    @Input() count: number;
    @Input() label: string;
    @Input() data: number;
    @Input() path: string;
    @Output() event: EventEmitter<any> = new EventEmitter();

    constructor(private router: Router) { }

    ngOnInit() {}

    onNavigate() {
      this.router.navigate([this.path]);
    }
}
