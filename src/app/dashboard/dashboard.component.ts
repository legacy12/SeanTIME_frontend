import { Quote } from './../shared/quote.model';
import { JobService } from './../shared/job.service';
import { SupplierService } from './../shared/supplier.service';
import { Supplier } from './../shared/supplier.model';
import { ContactService } from './../shared/contact.service';
import { StaffService } from './../shared/staff.service';
import { Contact } from './../shared/contact.model';
import { User } from '../shared/staff.model';
import { Job } from '../shared/job.model';
import { ClientService } from './../client.service';
import { Subscription } from 'rxjs/Subscription';
import { Client } from './../shared/client.model';
import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../router.animations';
import { QuoteService } from '../shared/quote.service';
import { InvoicingService } from '../shared/invoicing.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  animations: [routerTransition()]
})
export class DashboardComponent implements OnInit {
  public alerts: Array<any> = [];
  public sliders: Array<any> = [];

  clients: Client[];
  staffList: User[];
  contactList: Contact[];
  supplierList: Supplier[];
  jobsList: Job[];
  quoteList: Quote[];
  invoicingList: Job[];

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private supplierService: SupplierService,
    private jobService: JobService,
    private quoteService: QuoteService,
    private invoicingService: InvoicingService) {

    this.sliders.push({
      imagePath: 'assets/images/slider1.jpg',
      label: 'Office',
      text: 'An excellent place to work for all!.'
    }, {
        imagePath: 'assets/images/slider2.jpg',
        label: 'The company',
        text: 'Work hard for your fellow man.'
      }
    );

    this.alerts.push({
      id: 1,
      type: 'success',
      message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
    }, {
        id: 2,
        type: 'warning',
        message: `Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                Voluptates est animi quibusdam praesentium quam, et perspiciatis,
                consectetur velit culpa molestias dignissimos
                voluptatum veritatis quod aliquam! Rerum placeat necessitatibus, vitae dolorum`
      });
  }

  ngOnInit() {

    this.clientService.getClientList()
      .then(clients => this.clients = clients);

      this.staffService.getStaffList()
      .then(staffList => this.staffList = staffList);

    this.contactService.getContactList()
      .then(contactList => this.contactList = contactList);

    this.jobService.getJobList()
      .then(jobsList => this.jobsList = jobsList);

    this.supplierService.getSupplierList()
      .then(supplierList => this.supplierList = supplierList);

    this.quoteService.getQuoteList()
      .then(quoteList => this.quoteList = quoteList);

    this.invoicingService.getInvoicingList()
      .then(invoicingList => this.invoicingList = invoicingList);
  }

  public closeAlert(alert: any) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }
}
