import { Address } from './../../shared/address.model';
import { Quote } from './../../shared/quote.model';
import { Milestones } from './../../shared/milestones.model';
import { SupplierService } from './../../shared/supplier.service';
import { JobService } from './../../shared/job.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Job } from './../../shared/job.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Supplier } from '../../shared/supplier.model';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Item } from '../../shared/item.model';
import { ContactService } from '../../shared/contact.service';
import { QuoteService } from '../../shared/quote.service';
import { JobState } from '../../shared/jobstate.enum';

@Component({
  selector: 'app-copy-quote',
  templateUrl: './copy-quote.component.html',
  styleUrls: ['./copy-quote.component.css']
})
export class CopyQuoteComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };
  client: Client;
  quote: Quote;
  supplier: Supplier;
  staff: User;
  contact: Contact;
  id: string; // client ID
  quoteId: string; // job ID
  copyMode = false;
  quoteForm: FormGroup;
  staffList: User[];
  contactList: Contact[];
  supplierList: Supplier[];
  items: Item[];
  sellingPrice: number;
  markup: number;
  loading = true;
  deliveryAddress: string;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private supplierService: SupplierService,
    private quoteService: QuoteService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.quoteId = params['id'];
        this.id = params['cid'];
      }
      );

    if (this.quoteId != null) {
      this.copyMode = true;
    }

    Promise.all([this.staffService.getStaffList()
      .then(staffList => this.staffList = staffList),
    this.contactService.getContactList()
      .then(contactList => this.contactList = contactList),
    this.clientService.getClient(this.id)
      .then(client => this.client = client),
    this.quoteService.getQuote(this.quoteId)
      .then(quote => this.quote = quote),
    this.supplierService.getSupplierList()
      .then(supplierList => this.supplierList = supplierList)])
      .then(init => this.initForm())
      .then(log => console.log(this.supplierList))
      .then(id => this.quote.id = null)
      .then(loading => this.loading = false);
  }

  onSubmit() {

    console.log(this.quoteForm.value['items']);

    if (this.client.jobs == null) {
      this.client.jobs = new Array<Job>();
      console.log('new jobs array');
    }
    this.contact = this.quoteForm.value['contact'];
    this.staff = this.quoteForm.value['staff'];
    const displayTime = new Date();
    this.items = this.quoteForm.value['items'];
    console.log(this.items);
    console.log(this.staff);
    const milestones = new Milestones(displayTime);
    // While the backend still doesn't like full objects being sent this will have to do
    for (const item of this.items) {
      item.supplierId = item.supplier.id;
      item.supplier = null;
    }
    const newJob = new Quote(this.id,
      this.staff.id,
      this.contact.id,
      this.quoteForm.value['title'],
      this.quoteForm.value['description'],
      new Array<Note>(),
      this.quoteForm.value['purchasePrice'],
      this.quoteForm.value['sellingPrice'],
      this.markup,
      milestones,
      this.items,
      JobState.Quote,
      this.deliveryAddress
    );
    console.log(newJob);
    this.quoteService.addNewQuote(newJob)
      .then(cancel => this.onCancel());
  }

  getControls() {
    return (<FormArray>this.quoteForm.get('items')).controls;
  }

  onCancel() {
    this.router.navigate(['clients/', this.id]);
  }

  onDeleteItem(index: number) {
    (<FormArray>this.quoteForm.get('items')).removeAt(index);
  }

  onAddItem() {
    (<FormArray>this.quoteForm.get('items')).push(
      new FormGroup({
        'product': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ]),
        'supplier': new FormControl(null)
      })
    );
  }

  private initForm() {
    let clientName;
    let jobTitle;
    let jobContact;
    let jobDescription;
    let jobStaff;
    let purchasePrice;
    let sellingPrice;
    const items = new FormArray([]);
    if (this.copyMode) {
      clientName = this.client.name;
      jobTitle = this.quote.title;
      jobContact = this.quote.contact;
      jobDescription = this.quote.description;
      jobStaff = this.quote.user;
      purchasePrice = this.quote.purchasePrice;
      sellingPrice = this.quote.sellingPrice;
      this.markup = this.quote.markup;
      if (this.quote['items']) {
        for (const item of this.quote.items) {
          items.push(
            new FormGroup({
              'product': new FormControl(item.product, Validators.required),
              'amount': new FormControl(item.amount, [
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*$/)
              ]),
              'supplier': new FormControl(item.supplier)
            })
          );
        }
      }
    }

    this.quoteForm = new FormGroup({
      'title': new FormControl(jobTitle, Validators.required),
      'contact': new FormControl(jobContact, Validators.required),
      'description': new FormControl(jobDescription, Validators.required),
      'staff': new FormControl(jobStaff, Validators.required),
      'purchasePrice': new FormControl(purchasePrice, Validators.required),
      'customMarkup': new FormControl(this.markup),
      'sellingPrice': new FormControl(sellingPrice, Validators.required),
      'deliveryAddress': new FormControl(this.deliveryAddress, Validators.required),
      'items': items
    });

    const selectedStaff = this.staffList.find(staff => staff.id === this.quote.user.id);
    this.quote.user = selectedStaff;
    console.log(this.quote.user);
    this.quoteForm.controls['staff'].setValue(this.quote.user, { onlySelf: true });

    const selectedContact = this.contactList.find(contact => contact.id === this.quote.contactId);
    this.quote.contact = selectedContact;
    console.log(this.quote.contact);
    this.quoteForm.controls['contact'].setValue(this.quote.contact, { onlySelf: true });

    for (let i = 0; i < this.quote.items.length; i++) {
      const selectedSupplier = this.supplierList.find(supplier => supplier.id === this.quote.items[i].supplier.id);
      this.quote.items[i].supplier = selectedSupplier;
      console.log(selectedSupplier);
      (<FormArray>this.quoteForm.controls['items']).at(i).get('supplier').setValue(selectedSupplier);
    }
  }

  onMarkup(markup: number) {
    this.markup = markup;
    const purchasePrice = this.quoteForm.get('purchasePrice').value;
    this.sellingPrice = (purchasePrice + (purchasePrice * (markup / 100)));
    this.quoteForm.controls['sellingPrice'].setValue(purchasePrice + (purchasePrice * (markup / 100)));
    this.quoteForm.controls['customMarkup'].setValue('');
  }

  onCustomMarkup() {
    const purchasePrice = this.quoteForm.get('purchasePrice').value;
    this.markup = this.quoteForm.get('customMarkup').value;
    this.sellingPrice = (purchasePrice + (purchasePrice * (this.markup / 100)));
    this.quoteForm.controls['sellingPrice'].setValue(purchasePrice + (purchasePrice * (this.markup / 100)));
  }

  compareFn(s1: User, s2: User): boolean {
    return s1 && s2 ? s1.id === s2.id : s1 === s2;
  }

  onAddressComplete(address: Address) {
    this.deliveryAddress = address.formattedAddress;
  }

}
