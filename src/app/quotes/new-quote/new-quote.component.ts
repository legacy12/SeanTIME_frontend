import { PreItem } from './../../shared/pre-item.model';
import { Address } from './../../shared/address.model';
import { JobState } from './../../shared/jobstate.enum';
import { Milestones } from './../../shared/milestones.model';
import { Item } from './../../shared/item.model';
import { SupplierService } from './../../shared/supplier.service';
import { QuoteService } from './../../shared/quote.service';
import { ContactService } from './../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Quote } from './../../shared/quote.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Supplier } from '../../shared/supplier.model';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';

@Component({
  selector: 'app-new-quote',
  templateUrl: './new-quote.component.html',
  styleUrls: ['./new-quote.component.css']
})
export class NewQuoteComponent implements OnInit {
  model: NgbDateStruct;
  date: { year: number, month: number };
  client: Client;
  quote: Quote;
  staff: User;
  contact: Contact;
  id: string; // client ID
  quoteId: string; // Quote ID
  editMode = false;
  quoteForm: FormGroup;
  staffList: User[];
  contactList: Contact[];
  supplierList: Supplier[];
  sellingPrice: number;
  markup: number;
  loading = true;
  dueDate = new Date();
  items: Item[];
  preItemList: PreItem[];
  deliveryAddress: string;
  showAddressBook = false;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private supplierService: SupplierService,
    private quoteService: QuoteService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.quoteId = params['id'];
        this.id = params['cid'];
        console.log(this.quoteId);
      }
      );

    if (this.quoteId != null) {
      this.editMode = true;
    }

    Promise.all([this.staffService.getStaffList()
      .then(staffList => this.staffList = staffList),
    this.contactService.getClientContactList()
      .then(contactList => this.contactList = contactList),
    this.clientService.getClient(this.id)
      .then(client => this.client = client),
    this.quoteService.getQuote(this.quoteId)
      .then(Quote => this.quote = Quote)
      .then(quote => console.log(this.quote)),
    this.supplierService.getSupplierList()
      .then(supplierList => this.supplierList = supplierList)])
      .then(log => console.log(this.contactList))
      .then(init => this.initForm())
      .then(loading => this.loading = false);
  }

  onSubmit() {

    if (this.client.quotes == null) {
      this.client.quotes = new Array<Quote>();
    }

    if (this.editMode) {
      this.quote.title = this.quoteForm.value['title'];
      this.quote.contact = this.quoteForm.value['contact'];
      this.quote.description = this.quoteForm.value['description'];
      this.quote.user = this.quoteForm.value['staff'];
      this.quote.purchasePrice = this.quoteForm.value['purchasePrice'];
      this.quote.sellingPrice = this.quoteForm.value['sellingPrice'];
      this.quote.items = this.quoteForm.value['items'];
      this.quote.markup = this.markup;

      for (const item of this.quote.items) {
        item.supplierId = item.supplier.id;
        item.supplier = null;
      }

      console.log(this.quote);
      this.quoteService.updateQuote(this.quote)
        .then(cancel => this.onCancel());
    } else {
      this.contact = this.quoteForm.value['contact'];
      this.staff = this.quoteForm.value['staff'];
      this.items = this.quoteForm.value['items'];
      const displayTime = new Date();
      const milestones = new Milestones(displayTime);
      // While the backend still doesn't like full objects being sent this will have to do
      for (const item of this.items) {
        item.supplierId = item.supplier.id;
        item.supplier = null;
      }

      const newQuote = new Quote(this.id,
        this.staff.id,
        this.contact.id,
        this.quoteForm.value['title'],
        this.quoteForm.value['description'],
        new Array<Note>(),
        this.quoteForm.value['purchasePrice'],
        this.quoteForm.value['sellingPrice'],
        this.markup,
        milestones,
        this.items,
        JobState.Quote,
        this.deliveryAddress
      );
      console.log(newQuote);
      this.quoteService.addNewQuote(newQuote)
        .then(cancel => this.onCancel());
    }

  }

  getControls() {
    return (<FormArray>this.quoteForm.get('items')).controls;
  }

  onCancel() {
    this.router.navigate(['clients/', this.id]);
  }

  onDeleteItem(index: number) {
    (<FormArray>this.quoteForm.get('items')).removeAt(index);
  }

  onAddItem() {
    (<FormArray>this.quoteForm.get('items')).push(
      new FormGroup({
        'product': new FormControl(null, Validators.required),
        'amount': new FormControl(null, [
          Validators.required,
          Validators.pattern(/^[1-9]+[0-9]*$/)
        ]),
        'supplier': new FormControl(null)
      })
    );
  }


  private initForm() {
    let clientName;
    let QuoteTitle;
    let QuoteContact;
    let QuoteDescription;
    let QuoteStaff;
    let purchasePrice;
    let sellingPrice;
    let deliveryAddress;
    const items = new FormArray([]);

    if (this.editMode) {
      clientName = this.client.name;
      QuoteTitle = this.quote.title;
      QuoteContact = this.quote.contact;
      QuoteDescription = this.quote.description;
      QuoteStaff = this.quote.user.name;
      purchasePrice = this.quote.purchasePrice;
      sellingPrice = this.quote.sellingPrice;
      this.markup = this.quote.markup;
      deliveryAddress = this.quote.deliveryAddress;

      if (this.quote['items']) {
        for (const item of this.quote.items) {
          items.push(
            new FormGroup({
              'id': new FormControl(item.id),
              'jobId': new FormControl(item.jobId),
              'product': new FormControl(item.product, Validators.required),
              'amount': new FormControl(item.amount, [
                Validators.required,
                Validators.pattern(/^[1-9]+[0-9]*$/)
              ]),
              'supplier': new FormControl(item.supplier)
            })
          );
        }
      }
    }

    this.quoteForm = new FormGroup({
      'title': new FormControl(QuoteTitle, Validators.required),
      'contact': new FormControl(QuoteContact, Validators.required),
      'description': new FormControl(QuoteDescription, Validators.required),
      'staff': new FormControl(QuoteStaff, Validators.required),
      'purchasePrice': new FormControl(purchasePrice, Validators.required),
      'customMarkup': new FormControl(this.markup),
      'sellingPrice': new FormControl(sellingPrice, Validators.required),
      'addressControl': new FormControl(deliveryAddress, Validators.required),
      'selectedAddress': new FormControl(null, Validators.required),
      'items': items
    });

    if (this.editMode) {
      const selectedStaff = this.staffList.find(staff => staff.id === this.quote.user.id);
      this.quote.user = selectedStaff;
      console.log(this.quote.user);
      this.quoteForm.controls['staff'].setValue(this.quote.user, { onlySelf: true });

      const selectedContact = this.contactList.find(contact => contact.id === this.quote.contactId);
      this.quote.contact = selectedContact;
      console.log(this.quote.contact);
      this.quoteForm.controls['contact'].setValue(this.quote.contact, { onlySelf: true });

      for (let i = 0; i < this.quote.items.length; i++) {
        const selectedSupplier = this.supplierList.find(supplier => supplier.id === this.quote.items[i].supplier.id);
        this.quote.items[i].supplier = selectedSupplier;
        console.log(selectedSupplier);
        (<FormArray>this.quoteForm.controls['items']).at(i).get('supplier').setValue(selectedSupplier);
      }
    }
  }

  onMarkup(markup: number) {
    this.markup = markup;
    const purchasePrice = this.quoteForm.get('purchasePrice').value;
    this.sellingPrice = (purchasePrice + (purchasePrice * (markup / 100)));
    this.quoteForm.controls['sellingPrice'].setValue(purchasePrice + (purchasePrice * (markup / 100)));
    this.quoteForm.controls['customMarkup'].setValue('');
  }

  onCustomMarkup() {
    const purchasePrice = this.quoteForm.get('purchasePrice').value;
    this.markup = this.quoteForm.get('customMarkup').value;
    this.sellingPrice = (purchasePrice + (purchasePrice * (this.markup / 100)));
    this.quoteForm.controls['sellingPrice'].setValue(purchasePrice + (purchasePrice * (this.markup / 100)));
  }

  onAddressComplete(address: Address) {
    // TODO onAddressComplete gets triggered too early
    if (address != null) {
      this.deliveryAddress = address.formattedAddress;
      console.log(this.deliveryAddress);
      this.client.addresses.push(address);
      // this.clientService.updateClient(this.client);
    }
  }

  viewAddressBook() {
    this.showAddressBook = !this.showAddressBook;
  }

  onAddPreItem(preItems: PreItem[]) {
    console.log(preItems);
  }
}
