import { Quote } from './../../shared/quote.model';
import { JobService } from './../../shared/job.service';
import { ContactService } from './../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Job } from './../../shared/job.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { QuoteService } from '../../shared/quote.service';

@Component({
  selector: 'app-quote-detail',
  templateUrl: './quote-detail.component.html',
  styleUrls: ['./quote-detail.component.css']
})
export class QuoteDetailComponent implements OnInit {
  jobForm: FormGroup;
  quoteId: string; // job ID
  client: Client;
  id: string; // client ID
  editMode = false;
  quote: Quote;
  butDisabled = true;
  loading = true;
  dueDate = null;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private quoteService: QuoteService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router,
    public dialog: MatDialog) {
  }


  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.quoteId = params['id'];
        if (this.quoteId != null) {
          this.quoteService.getQuote(this.quoteId)
            .then(quote => this.quote = quote)
            .then(log => console.log(this.quote))
            .then(client => this.client = this.quote.client)
            .then(init => this.initForm())
            .then(loading => this.loading = false)
            .then(clientId => this.id = this.quote.client.id);
        } else {
          this.loading = false;
          this.initForm();
        }
      }
      );
  }

  private initForm() {
    let quoteTitle = '';
    let quoteContact = '';
    let quoteDescription = '';
    let quoteStaff = '';
    let client = '';
    let purchasePrice = 0;
    let sellingPrice = 0;
    let markup = 0;
    let friendlyId = 0;

    quoteTitle = this.quote.title;
    quoteContact = this.quote.contact.name;
    quoteDescription = this.quote.description;
    quoteStaff = this.quote.user.name;
    purchasePrice = this.quote.purchasePrice;
    sellingPrice = this.quote.sellingPrice;
    markup = this.quote.markup;
    client = this.quote.client.name;
    friendlyId = this.quote.friendlyId;

    this.jobForm = new FormGroup({
      'title': new FormControl(quoteTitle, Validators.required),
      'contact': new FormControl(quoteContact, Validators.required),
      'description': new FormControl(quoteDescription, Validators.required),
      'staff': new FormControl(quoteStaff, Validators.required),
      'purchasePrice': new FormControl(purchasePrice, Validators.required),
      'sellingPrice': new FormControl(sellingPrice, Validators.required),
      'client': new FormControl(client, Validators.required),
      'markup': new FormControl(markup, Validators.required),
      'friendlyId': new FormControl(friendlyId, Validators.required)
    });

    this.jobForm.get('staff').disable();
    this.jobForm.get('contact').disable();
    this.jobForm.get('client').disable();
  }

  onUpgrade() {
      // TODO This works but need to make it look much nicer
      if (window.confirm('Are you sure you want convert this quote to a job?')) {
        this.openDialog();
      }
  }

  onEdit() {
    console.log('tet');
    this.router.navigate(['quotes/edit/' + this.client.id + '/' + this.quote.id]);
  }

  onCopy() {
    console.log('btb');
    this.router.navigate(['quotes/copy/' + this.client.id + '/' + this.quote.id]);
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(UpgradeModal, {
      width: '260px',
      data: { date: this.dueDate }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dueDate = result;
      console.log(this.dueDate);
      if (this.dueDate != null) {
        this.quote.milestones.due = this.dueDate;

        for (let item of this.quote.items) {
          item.supplierId = item.supplier.id;
          item.supplier = null;
        }

        this.quoteService.upgradeQuote(this.quote);
        this.onCancel();
      }
    });
  }

  onCancel() {
    this.router.navigate(['clients/' + this.id]);
  }

}

@Component({
  selector: 'dialog-overview',
  templateUrl: 'upgrade-modal.html',
})
export class UpgradeModal {

  constructor(
    public dialogRef: MatDialogRef<UpgradeModal>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
