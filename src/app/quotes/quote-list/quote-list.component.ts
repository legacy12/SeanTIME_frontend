import { Client } from './../../shared/client.model';
import { Quote } from './../../shared/quote.model';
import { Component, OnInit } from '@angular/core';
import { QuoteService } from '../../shared/quote.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-quote-list',
  templateUrl: './quote-list.component.html',
  styleUrls: ['./quote-list.component.css']
})
export class QuoteListComponent implements OnInit {
  quoteList: Quote[];
  client: Client;
  loading = true;

  constructor(private quoteService: QuoteService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.quoteService.getQuoteList()
    .then(quoteList => this.quoteList = quoteList)
    .then(loading => this.loading = false)
    .then(log => console.log(this.quoteList));
  }

  onSelectQuote(index: string) {
    this.router.navigate([index], {relativeTo: this.route});
  }

  onNewQuote() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }
}
