import { SupplierContactItemComponent } from './../shared/components/supplier-contact-item/supplier-contact-item.component';
import { NewPreProductionComponent } from './../pre-production/new-pre-production/new-pre-production.component';
import { PreProductionDetailComponent } from './../pre-production/pre-production-detail/pre-production-detail.component';
import { UpgradeModalComponent } from './../pre-production/pre-production-detail/pre-production-detail.component';
import { PreProductionComponent } from './../pre-production/pre-production.component';
import { PreItemListComponent } from './../shared/components/pre-item-list/pre-item-list.component';
import { PreItemComponent } from './../shared/components/pre-item/pre-item.component';
import { AddressService } from './../shared/address.service';
import { AddressSelectorComponent } from './../shared/components/address-selector/address-selector.component';
import { AddressBookComponent } from './../shared/components/address-book/address-book.component';
import { InputAddressComponent } from './../shared/components/input-address/input-address.component';
import { SupplierDetailComponent, NonConformanceComponent } from './../suppliers/supplier-detail/supplier-detail.component';
import { InvoicingListComponent } from './../invoicing/invoicing-list/invoicing-list.component';
import { QuoteDetailComponent, UpgradeModal } from './../quotes/quote-detail/quote-detail.component';
import { QuoteListComponent } from './../quotes/quote-list/quote-list.component';
import { JobLineChartComponent } from './../shared/components/job-line-chart/job-line-chart.component';
import { JobListComponent } from './../jobs/job-list/job-list.component';
import { JobService } from './../shared/job.service';
import { DropdownDirective } from './../shared/dropdown.directive';
import { CommonModule } from '@angular/common';
import { AppComponent } from './../app.component';
import { LayoutRoutingModule } from './layout-routing.module';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guard/auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ContactService } from '../shared/contact.service';
import { StaffEditComponent } from '../staff/staff-edit/staff-edit.component';
import { StaffItemComponent } from '../staff/staff-item/staff-item.component';
import { StaffService } from '../shared/staff.service';
import { StaffListComponent } from '../staff/staff-list/staff-list.component';
import { MessageService } from '../shared/message.service';
import { SupplierService } from '../shared/supplier.service';
import { NavMenuComponent } from '../navmenu/navmenu.component';
import { ClientService } from '../client.service';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

// Prime NG

import { HeaderComponent } from '../header/header.component';
import { OrdersComponent } from '../orders/orders.component';
import { ClientsComponent } from '../clients/clients.component';
import { ClientListComponent } from '../clients/client-list/client-list.component';
import { ClientItemComponent } from '../clients/client-list/client-item/client-item.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { JobsComponent } from '../jobs/jobs.component';
import { ClientDetailComponent } from '../clients/client-list/client-item/client-detail/client-detail.component';
import { NewJobComponent } from '../jobs/new-job/new-job.component';
import { ClientEditComponent } from '../clients/client-list/client-item/client-edit/client-edit.component';
import { StaffComponent } from '../staff/staff.component';
import { StaffDetailComponent } from '../staff/staff-detail/staff-detail.component';
import { ContactsComponent } from '../contacts/contacts.component';
import { ContactListComponent } from '../contacts/contact-list/contact-list.component';
import { ContactItemComponent } from '../contacts/contact-item/contact-item.component';
import { ContactEditComponent } from '../contacts/contact-edit/contact-edit.component';
import { ContactDetailComponent } from '../contacts/contact-detail/contact-detail.component';
import { StatComponent } from '../dashboard/stat/stat.component';
import { NotificationComponent } from '../dashboard/components/notification/notification.component';
import { TimelineComponent } from '../shared/components/timeline/timeline.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JobDetailComponent } from '../jobs/job-detail/job-detail.component';
import { SuppliersComponent } from '../suppliers/suppliers.component';
import { SupplierListComponent } from '../suppliers/supplier-list/supplier-list.component';
import { SupplierEditComponent } from '../suppliers/supplier-edit/supplier-edit.component';
import { LayoutComponent } from './layout.component';
import { ChartModule } from 'primeng/primeng';
import { InvoicingComponent } from '../invoicing/invoicing.component';
import { SalesComponent } from '../sales/sales.component';
import { QuotesComponent } from '../quotes/quotes.component';
import { QuoteService } from '../shared/quote.service';
import { NewQuoteComponent } from '../quotes/new-quote/new-quote.component';
import { CopyJobComponent } from '../jobs/copy-job/copy-job.component';
import { MatButtonModule, MatCheckboxModule, MatDatepickerModule, MatFormFieldModule, MatNativeDateModule,
  MatInputModule, MatDialogModule} from '@angular/material';
import { CopyQuoteComponent } from '../quotes/copy-quote/copy-quote.component';
import { InvoicingService } from '../shared/invoicing.service';
import { AgmCoreModule } from '@agm/core';
import { PreProductionListComponent } from '../pre-production/pre-production-list/pre-production-list.component';
import { PreProductionService } from '../shared/pre-production.service';

@NgModule({
  declarations: [
    HeaderComponent,
    OrdersComponent,
    ClientsComponent,
    ClientListComponent,
    ClientItemComponent,
    DashboardComponent,
    JobsComponent,
    ClientDetailComponent,
    NewJobComponent,
    ClientEditComponent,
    StaffComponent,
    StaffListComponent,
    StaffItemComponent,
    StaffDetailComponent,
    StaffEditComponent,
    ContactsComponent,
    ContactListComponent,
    ContactItemComponent,
    ContactEditComponent,
    ContactDetailComponent,
    StatComponent,
    DashboardComponent,
    StatComponent,
    NotificationComponent,
    TimelineComponent,
    JobDetailComponent,
    SuppliersComponent,
    SupplierListComponent,
    SupplierEditComponent,
    LayoutComponent,
    NavMenuComponent,
    DropdownDirective,
    JobListComponent,
    JobLineChartComponent,
    InvoicingComponent,
    SalesComponent,
    QuotesComponent,
    QuoteListComponent,
    NewQuoteComponent,
    CopyJobComponent,
    QuoteDetailComponent,
    UpgradeModal,
    UpgradeModalComponent,
    CopyQuoteComponent,
    InvoicingListComponent,
    SupplierDetailComponent,
    NonConformanceComponent,
    InputAddressComponent,
    AddressBookComponent,
    AddressSelectorComponent,
    PreItemComponent,
    PreItemListComponent,
    PreProductionComponent,
    PreProductionListComponent,
    PreProductionDetailComponent,
    NewPreProductionComponent,
    SupplierContactItemComponent
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyD87ucMTUaPW73ZjzwnpQM6toizVNmFUn8',
      libraries: ['places']
    }),
    ReactiveFormsModule,
    NgbModule.forRoot(),
    FormsModule,
    LayoutRoutingModule,
    CommonModule,
    ChartModule,
    MatButtonModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatNativeDateModule,
    MatInputModule,
    MatDialogModule
  ],
  entryComponents: [UpgradeModalComponent, UpgradeModal, NonConformanceComponent],

  providers: [ClientService, ContactService, QuoteService, SupplierService, JobService,
    AddressService, InvoicingService, PreProductionService, AuthGuard],
})

export class LayoutModule { }
