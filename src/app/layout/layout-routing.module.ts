import { PreProductionDetailComponent } from './../pre-production/pre-production-detail/pre-production-detail.component';
import { NewPreProductionComponent } from './../pre-production/new-pre-production/new-pre-production.component';
import { ClientEditComponent } from './../clients/client-list/client-item/client-edit/client-edit.component';
import { AddressBookComponent } from './../shared/components/address-book/address-book.component';
import { SupplierDetailComponent } from './../suppliers/supplier-detail/supplier-detail.component';
import { QuoteDetailComponent } from './../quotes/quote-detail/quote-detail.component';
import { CopyJobComponent } from './../jobs/copy-job/copy-job.component';
import { QuotesComponent } from './../quotes/quotes.component';
import { SalesComponent } from './../sales/sales.component';
import { InvoicingComponent } from './../invoicing/invoicing.component';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from '../shared/guard/auth.guard';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgbAlertModule, NgbCarouselModule } from '@ng-bootstrap/ng-bootstrap';
import { ContactService } from '../shared/contact.service';
import { StaffEditComponent } from '../staff/staff-edit/staff-edit.component';
import { StaffItemComponent } from '../staff/staff-item/staff-item.component';
import { StaffService } from '../shared/staff.service';
import { StaffListComponent } from '../staff/staff-list/staff-list.component';
import { MessageService } from '../shared/message.service';
import { SupplierService } from '../shared/supplier.service';
import { NavMenuComponent } from '../navmenu/navmenu.component';
import { ClientService } from '../client.service';
import { AppRoutingModule } from '../app-routing.module';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from '../app.component';
import { HeaderComponent } from '../header/header.component';
import { OrdersComponent } from '../orders/orders.component';
import { ClientsComponent } from '../clients/clients.component';
import { ClientListComponent } from '../clients/client-list/client-list.component';
import { ClientItemComponent } from '../clients/client-list/client-item/client-item.component';
import { DashboardComponent } from '../dashboard/dashboard.component';
import { JobsComponent } from '../jobs/jobs.component';
import { ClientDetailComponent } from '../clients/client-list/client-item/client-detail/client-detail.component';
import { NewJobComponent } from '../jobs/new-job/new-job.component';
import { StaffComponent } from '../staff/staff.component';
import { StaffDetailComponent } from '../staff/staff-detail/staff-detail.component';
import { ContactsComponent } from '../contacts/contacts.component';
import { ContactListComponent } from '../contacts/contact-list/contact-list.component';
import { ContactItemComponent } from '../contacts/contact-item/contact-item.component';
import { ContactEditComponent } from '../contacts/contact-edit/contact-edit.component';
import { ContactDetailComponent } from '../contacts/contact-detail/contact-detail.component';
import { StatComponent } from '../dashboard/stat/stat.component';
import { NotificationComponent } from '../dashboard/components/notification/notification.component';
import { TimelineComponent } from '../shared/components/timeline/timeline.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { JobDetailComponent } from '../jobs/job-detail/job-detail.component';
import { SuppliersComponent } from '../suppliers/suppliers.component';
import { SupplierListComponent } from '../suppliers/supplier-list/supplier-list.component';
import { SupplierEditComponent } from '../suppliers/supplier-edit/supplier-edit.component';
import { LoginComponent } from '../login/login.component';
import { LayoutComponent } from '../layout/layout.component';
import { NewQuoteComponent } from '../quotes/new-quote/new-quote.component';
import { CopyQuoteComponent } from '../quotes/copy-quote/copy-quote.component';

const routes: Routes = [
  {
    path: '', component: LayoutComponent,
    children: [
      { path: 'orders', component: OrdersComponent },
      { path: 'clients/new', component: ClientEditComponent },    // This shouldn't be needed but is
      { path: 'clients/:id', component: ClientDetailComponent },  // This shouldn't be needed but is
      { path: 'clients/:cid/job/new', component: NewJobComponent },  // This shouldn't be needed but is
      { path: 'clients/:cid/quote/new', component: NewPreProductionComponent },  // This shouldn't be needed but is
      { path: 'clients/edit/:id', component: ClientEditComponent },
      {
        path: 'clients', component: ClientsComponent, children: [
          { path: 'new', component: ClientEditComponent },
          { path: 'edit/:id', component: ClientEditComponent },
          { path: 'list', component: ClientListComponent },
          { path: ':id', component: ClientDetailComponent },
          { path: ':cid/new', component: NewJobComponent },
        ]
      },
      { path: 'dashboard', component: DashboardComponent },
      { path: 'contacts/new', component: ContactEditComponent }, // This shouldn't be needed but is
      { path: 'contacts/edit/:id', component: ContactEditComponent }, // This shouldn't be needed but is
      { path: 'contacts/:id', component: ContactDetailComponent }, // This shouldn't be needed but is
      {
        path: 'contacts', component: ContactsComponent, children: [
          { path: 'new', component: ContactEditComponent },
          { path: ':id', component: ContactDetailComponent }
        ]
      },
      { path: 'jobs/new', component: NewJobComponent }, // This shouldn't be needed to get to a new job - but is
      { path: 'jobs/edit/:cid/:id', component: NewJobComponent }, // This shouldn't be needed to get to a new job - but is
      { path: 'jobs/copy/:cid/:id', component: CopyJobComponent }, // This shouldn't be needed to get to a new job - but is
      { path: 'jobs/:id', component: JobDetailComponent }, // This shouldn't be needed to get to a new job - but is
      {
        path: 'jobs', component: JobsComponent, children: [
          { path: 'new', component: NewJobComponent },
          { path: 'edit/:id', component: NewJobComponent },
          { path: ':id', component: JobDetailComponent },
        ]
      },
      { path: 'quotes/edit/:cid/:id', component: NewQuoteComponent }, // This shouldn't be needed to get to a new job - but is
      { path: 'quotes/copy/:cid/:id', component: CopyQuoteComponent }, // This shouldn't be needed to get to a new job - but is
      { path: 'staff/new', component: StaffEditComponent },
      { path: 'staff/:id', component: StaffDetailComponent },
      {
        path: 'staff', component: StaffComponent, children: [
          { path: 'new', component: StaffEditComponent },
          { path: ':id', component: StaffDetailComponent }
        ]
      },
      { path: 'settings', component: DashboardComponent },
      { path: 'invoicing', component: InvoicingComponent },
      { path: 'sales', component: QuotesComponent },
      { path: 'quotes/:id', component: QuoteDetailComponent},
      { path: 'suppliers/new', component: SupplierEditComponent }, // This shouldn't be needed but is
      { path: 'suppliers/:id', component: SupplierDetailComponent }, // This shouldn't be needed but is
      { path: 'suppliers/:sid/addresses', component: AddressBookComponent },
      {
        path: 'suppliers', component: SuppliersComponent, children: [
          { path: 'new', component: SupplierEditComponent }
        ]
      },
      { path: 'preprod/:id', component: PreProductionDetailComponent },
      { path: 'preprod/edit/:cid/:id', component: NewPreProductionComponent },
    ]
  }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LayoutRoutingModule { }
