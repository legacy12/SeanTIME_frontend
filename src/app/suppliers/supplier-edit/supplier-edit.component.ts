import { Issue } from './../../shared/issue.model';
import { ContactRole } from '../../shared/contact-role.enum';
import { Subscription } from 'rxjs/Rx';
import { Client } from '../../shared/client.model';
import { Response } from '@angular/http';
import { ContactService } from './../../shared/contact.service';
import { SupplierService } from './../../shared/supplier.service';
import { Contact } from './../../shared/contact.model';
import { Supplier } from './../../shared/supplier.model';
import { MessageService } from '../../shared/message.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { StaffService } from './../../shared/staff.service';
import { ClientService } from '../../client.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-supplier-edit',
  templateUrl: './supplier-edit.component.html',
  styleUrls: ['./supplier-edit.component.css']
})
export class SupplierEditComponent implements OnInit {
  editMode = false;
  supplierForm: FormGroup;
  contactList: Contact[];

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private supplierService: SupplierService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.contactService.getUnassignedRoledContactList(ContactRole.Supplier)
    .then(contactList => this.contactList = contactList);

    this.initForm();

  }

  onSubmit() {
    const contacts: Contact[] = this.supplierForm.value['contact'];
    const newSupplier = new Supplier(
      this.supplierForm.value['name'],
      this.supplierForm.value['phone'],
      contacts,
      new Array<Issue>()
    );

    if (this.editMode) {

    } else {
      this.supplierService.addNewSupplier(newSupplier)
      .then(cancel => this.onCancel());
    }
  }

  onCancel() {
    this.router.navigate(['suppliers/']);
  }

  private initForm() {
    const supplierName = '';
    const supplierPhone = '';
    const supplierContact = '';

    if (this.editMode) {
    }

    this.supplierForm = new FormGroup({
      'name': new FormControl(supplierName, Validators.required),
      'phone': new FormControl(supplierPhone, Validators.required),
      'contact': new FormControl(supplierContact, Validators.required)
    });
  }
}
