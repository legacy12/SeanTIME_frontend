import { Component, OnInit } from '@angular/core';
import { Supplier } from './../../shared/supplier.model';
import { Response } from '@angular/http';
import { ContactService } from '../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from './../../shared/staff.service';
import { SupplierService } from './../../shared/supplier.service';
import { ClientService } from './../../client.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.css']
})
export class SupplierListComponent implements OnInit {
  supplierList: Supplier[];
  loading = true;

  constructor(private staffService: StaffService,
              private contactService: ContactService,
              private supplierService: SupplierService,
              private router: Router,
              private route: ActivatedRoute) {
              }

  ngOnInit() {
    this.supplierService.getSupplierList()
    .then(supplierList => this.supplierList = supplierList)
    .then(log => console.log(this.supplierList))
    .then(loading => this.loading = false);
  }

  onNewSupplier() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  onSelectSupplier(index: number) {
    this.router.navigate([index], {relativeTo: this.route});
  }
}
