import { Supplier } from './../../shared/supplier.model';
import { SupplierService } from './../../shared/supplier.service';
import { JobService } from './../../shared/job.service';
import { ContactService } from './../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Job } from './../../shared/job.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Issue } from '../../shared/issue.model';

@Component({
  selector: 'app-supplier-detail',
  templateUrl: './supplier-detail.component.html',
  styleUrls: ['./supplier-detail.component.css']
})
export class SupplierDetailComponent implements OnInit {
  supplierForm: FormGroup;
  supplierId: string; // job ID
  id: string; // client ID
  editMode = false;
  supplier: Supplier;
  butDisabled = true;
  loading = true;
  issue: string;

  constructor(private supplierService: SupplierService,
    private route: ActivatedRoute,
    private router: Router,
    private dialog: MatDialog) {
  }


  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.supplierId = params['id'];
        if (this.supplierId != null) {
          this.supplierService.getSupplier(this.supplierId)
            .then(supplier => this.supplier = supplier)
            .then(log => console.log(this.supplier))
            .then(init => this.initForm())
            .then(loading => this.loading = false);
        } else {
          this.loading = false;
          this.initForm();
        }
      }
      );
  }

  private initForm() {
    let supplierName = '';
    let supplierContact = '';
    let supplierPhoneNumber;

    supplierName = this.supplier.name;
    supplierContact = this.supplier.contacts[0].name;
    supplierPhoneNumber = this.supplier.phoneNumber;

    this.supplierForm = new FormGroup({
      'name': new FormControl(supplierName, Validators.required),
      'contact': new FormControl(supplierContact, Validators.required),
      'phoneNumber': new FormControl(supplierPhoneNumber, Validators.required),
    });

    this.supplierForm.get('contact').disable();

  }

  onSubmit() {

  }

  onCancel() {
    this.router.navigate(['suppliers']);
  }

  onEdit() {
    this.router.navigate(['suppliers/edit/' + this.supplier.id]);
  }

  onAddNonConformance() {

    if (window.confirm('Are you sure you want to add an issue to this supplier?')) {
      this.openDialog();
    }

  }

  onAddressBook() {
    this.router.navigate(['suppliers/' + this.supplierId + '/addresses']);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(NonConformanceComponent, {
      width: '260px',
      data: { date: this.issue }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.issue = result;
      console.log(this.issue);
      if (this.issue != null) {
        const theIssue = new Issue(new Date(), this.issue);
        this.supplier.issues.push(theIssue);
        this.supplierService.updateSupplier(this.supplier)
        .then(cancel => this.onCancel());
      }
    });
  }

}


@Component({
  selector: 'app-non-conformance',
  templateUrl: 'non-conformance-modal.html',
  styleUrls: ['./supplier-detail.component.css']
})
export class NonConformanceComponent {

  constructor(
    public dialogRef: MatDialogRef<NonConformanceComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
