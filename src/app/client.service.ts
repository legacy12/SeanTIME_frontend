import { environment } from './../environments/environment';
import { Http } from '@angular/http';
import { Client } from './shared/client.model';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Job } from './shared/job.model';

@Injectable()
export class ClientService {
  private clientsList: Client[] = new Array<Client>();

   constructor(private http: Http) { }

     updateClient(client: Client): Promise<Client> {
       return this.http.put(environment.apiUrl + '/api/Client/' + client.id, client)
         .toPromise()
         .then(response => response.json() as Client)
         .catch(this.handleError);
     }

     getClient(id: string): Promise<Client> {
       return this.http.get(environment.apiUrl + '/api/Client/' + id)
         .toPromise()
         .then(response => response.json() as Client)
         .catch(this.handleError);
     }

     getClientList() {
       return this.http.get(environment.apiUrl + '/api/Client')
         .toPromise()
         .then(response => response.json().items as Client[])
         .catch(this.handleError);
     }

     addNewClient(client: Client) {
       console.log(JSON.parse(JSON.stringify(client)));

       return this.http.post(environment.apiUrl + '/api/Client', JSON.parse(JSON.stringify(client)))
       .toPromise()
       .then(response => response.json())
       .catch(this.handleError);
     }

     deleteClient(index: string) {
       console.log(index);
       return this.http.delete(environment.apiUrl + '/api/Client/' + index);
     }

     updateClientOnStop(client: Client): Promise<Client> {
      return this.http.put(environment.apiUrl + '/api/Client/' + client.id + '/OnStop', client)
        .toPromise()
        .then(response => response.json() as Client)
        .catch(this.handleError);
    }

     private handleError(error: any): Promise<any> {
       console.error('An error occurred', error); // for demo purposes only
       return Promise.reject(error.message || error);
     }

}
