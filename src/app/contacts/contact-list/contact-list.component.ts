import { ContactRole } from './../../shared/contact-role.enum';
import { Response } from '@angular/http';
import { ContactService } from '../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { ActivatedRoute, Router } from '@angular/router';
import { StaffService } from './../../shared/staff.service';
import { ClientService } from './../../client.service';
import { Subscription } from 'rxjs/Subscription';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {
  contactList: Contact[];
  subscription: Subscription;
  loading = true;
  ContactRole: typeof ContactRole = ContactRole;

  constructor(private clientService: ClientService,
              private staffService: StaffService,
              private contactService: ContactService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.contactService.getContactList()
    .then(contactList => this.contactList = contactList)
    .then(loading => this.loading = false);
  }

  onNewContact() {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

  onSelectContact(index: number) {
    this.router.navigate([index], {relativeTo: this.route});
  }

  onGetStaffList() {
    this.contactService.getContactList()
    .then(contactList => this.contactList = contactList);  }
}
