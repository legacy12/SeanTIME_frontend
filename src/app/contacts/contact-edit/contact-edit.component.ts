import { Note } from './../../shared/note.model';
import { Job } from './../../shared/job.model';
import { Client } from '../../shared/client.model';
import { Response } from '@angular/http';
import { ContactService } from './../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { MessageService } from '../../shared/message.service';
import { ActivatedRoute, Router, Params } from '@angular/router';
import { StaffService } from './../../shared/staff.service';
import { ClientService } from '../../client.service';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Component, OnInit, Pipe, PipeTransform } from '@angular/core';

@Component({
  selector: 'app-contact-edit',
  templateUrl: './contact-edit.component.html',
  styleUrls: ['./contact-edit.component.css'],
})

export class ContactEditComponent implements OnInit {
  editMode = false;
  loading = true;
  contactForm: FormGroup;
  id: string; // Contact ID
  contact: Contact;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params['id'];
          console.log(this.id);
          if (this.id != null) {
            this.contactService.getContact(this.id)
              .then(contact => this.contact = contact)
              .then(loading => this.loading = false)
              .then(init => this.initForm());
          } else {
            this.loading = false;
            this.initForm();
          }
        }
      );
  }

  onSubmit() {
    const newContact = new Contact(
      this.contactForm.value['name'],
      this.contactForm.value['phone'],
      new Array<Note>(), // TODO Fix this
      this.contactForm.value['email'],
      this.contactForm.value['contactRole'],
      new Array<Job>(),
      false// TODO Fix this
    );

    if (this.editMode) {
      newContact.id = this.contact.id;
      this.contactService.updateContact(newContact)
        .then(cancel => this.onCancel());
    } else {
      this.contactService.addNewContact(newContact)
        .then(cancel => this.onCancel());
    }

  }

  onCancel() {
    this.router.navigate(['contacts/']);
  }

  private initForm() {

    if (this.contact != null) {
      console.log('edit contact');
      this.editMode = true;
    }

    let contactName = '';
    let contactPhone;
    let contactRole;
    let contactEmail = '';

    if (this.editMode) {
      contactName = this.contact.name;
      contactPhone = this.contact.phoneNumber;
      contactRole = this.contact.contactRole;
      contactEmail = this.contact.email;
    }

    this.contactForm = new FormGroup({
      'name': new FormControl(contactName, Validators.required),
      'phone': new FormControl(contactPhone, Validators.required),
      'contactRole': new FormControl(contactRole, Validators.required),
      'email': new FormControl(contactEmail, Validators.email)
    });
  }
}
