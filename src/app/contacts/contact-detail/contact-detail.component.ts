import { ContactRole } from './../../shared/contact-role.enum';
import { Response } from '@angular/http';
import { MessageService } from '../../shared/message.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { ContactService } from '../../shared/contact.service';
import { StaffService } from '../../shared/staff.service';
import { ClientService } from '../../client.service';
import { Subscription } from 'rxjs/Rx';
import { Contact } from '../../shared/contact.model';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-contact-detail',
  templateUrl: './contact-detail.component.html',
  styleUrls: ['./contact-detail.component.css']
})
export class ContactDetailComponent implements OnInit {
  contact: Contact;
  id: string;
  subscription: Subscription;
  contactForm: FormGroup;
  editmode = false;
  loading = true;
  ContactRole: typeof ContactRole = ContactRole;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.id = params['id'];
        console.log(this.id);
        if (this.id != null) {
          this.contactService.getContact(this.id)
          .then(contact => this.contact = contact)
          .then(loading => this.loading = false)
          .then(init => this.initForm());
        } else {
          this.loading = false;
          this.initForm();
        }
      }
      );
  }

  private initForm() {
    const contactName = '';
    const contactEmail = '';
    const contactPhoneNo = '';
    const contactRole = '';

    this.contactForm = new FormGroup({
      'name': new FormControl(contactName, Validators.required),
      'email': new FormControl(contactEmail, Validators.required),
      'phone': new FormControl(contactEmail, Validators.required),
      'role': new FormControl(contactRole, Validators.required),
    });
  }

  onEdit() {
    this.router.navigate(['contacts/edit/' + this.contact.id]);
  }

  onDelete() {
    console.log('would delete' + this.contact.id);
    this.contactService.deleteContact(this.contact.id)
      .subscribe(
      (response: Response) => {
        console.log(response);
        this.router.navigate(['contacts']);
      }
      );
  }
}
