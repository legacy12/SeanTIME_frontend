import { Client } from './../../../shared/client.model';
import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-client-item',
  templateUrl: './client-item.component.html',
  styleUrls: ['./client-item.component.css']
})
export class ClientItemComponent implements OnInit {
  @Input() client: Client;
  @Input() index: number;

  ngOnInit() {
  }

}
