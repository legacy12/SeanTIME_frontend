import { User } from './../../../../shared/staff.model';
import { StaffService } from './../../../../shared/staff.service';
import { Address } from './../../../../shared/address.model';
import { Quote } from './../../../../shared/quote.model';
import { ContactRole } from '../../../../shared/contact-role.enum';
import { ContactService } from '../../../../shared/contact.service';
import { Contact } from '../../../../shared/contact.model';
import { MessageService } from './../../../../shared/message.service';
import { Job } from './../../../../shared/job.model';
import { ClientService } from './../../../../client.service';
import { Client } from './../../../../shared/client.model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';

@Component({
  selector: 'app-client-edit',
  templateUrl: './client-edit.component.html',
  styleUrls: ['./client-edit.component.css']
})
export class ClientEditComponent implements OnInit {
  client: Client;
  id: string;
  loading = true;
  editMode = false;
  clientForm: FormGroup;
  clientContactList: Contact[];
  staffList: User[];
  address: Address;

  constructor(private clientService: ClientService,
    private contactService: ContactService,
    private staffService: StaffService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = params['id'];
          console.log(this.id);
          if (this.id != null) {
            this.editMode = true;
            console.log(this.id);
            Promise.all([this.clientService.getClient(this.id)
              .then(client => this.client = client)
              .then(client => console.log(this.client)),
            this.staffService.getStaffList()
              .then(staffList => this.staffList = staffList)
              .then(log => console.log(this.staffList)),
            this.contactService.getContactList()
              .then(contactList => this.clientContactList = contactList)
              .then(init => this.initForm())
              .then(loading => this.loading = false)]);
          } else {
            Promise.all([this.contactService.getUnassignedRoledContactList(ContactRole.Client)
              .then(contactList => this.clientContactList = contactList),
              this.staffService.getStaffList()
              .then(staffList => this.staffList = staffList)
              .then(log => console.log(this.staffList))
              .then(init => this.initForm())
              .then(loading => this.loading = false)]);
          }
        }
      );
  }

  onSubmit() {

    const addresses = new Array<Address>();
    const contact = this.clientForm.value['contact'];
    const staff = this.clientForm.value['staff'];

    if (this.editMode) {
      this.client.name = this.clientForm.value['name'];
      this.client.description = this.clientForm.value['description'];
      this.client.contactId = contact.id;
      this.client.userId = staff.id;
      this.client.user = null;  // TODO fix this
      this.clientService.updateClient(this.client)
        .then(cancel => this.onCancel());
    } else {
      const newClient = new Client(
        this.clientForm.value['name'],
        this.clientForm.value['description'],
        new Array<Job>(),
        new Array<Quote>(),
        contact,
        staff.id,
        addresses
      );

      this.clientService.addNewClient(newClient)
        .then(cancel => this.onCancel());
    }
  }

  onCancel() {
    if (this.editMode) {
      this.router.navigate(['clients/' + this.id]);
    } else {
      this.router.navigate(['clients/']);
    }
  }

  private initForm() {
    let clientName = '';
    let clientDescription = '';
    let clientContact;
    let clientStaffMember;

    if (this.editMode) {
      clientName = this.client.name;
      clientDescription = this.client.description;
      clientContact = this.client.contact;
      clientStaffMember = this.client.user;
    }

    this.clientForm = new FormGroup({
      'name': new FormControl(clientName, Validators.required),
      'contact': new FormControl(clientContact, Validators.required),
      'staff': new FormControl(clientStaffMember, Validators.required),
      'description': new FormControl(clientDescription, Validators.required),
    });

    if (this.editMode) {
      const selectedContact = this.clientContactList.find(contact => contact.id === this.client.contact.id);
      this.client.contact = selectedContact;
      this.clientForm.controls['contact'].setValue(this.client.contact, { onlySelf: true });

      const selectedUser = this.staffList.find(staff => staff.id === this.client.user.id);
      this.client.user = selectedUser;
      this.clientForm.controls['staff'].setValue(this.client.user, { onlySelf: true });
    }
  }

  onStop(onStop: Boolean) {
    this.client.onStop = onStop;
    this.clientService.updateClientOnStop(this.client);
  }

  onAddressComplete(address: Address) {
    this.address = address;
  }
}
