import { JobState } from './../../../../shared/jobstate.enum';
import { Response } from '@angular/http';
import { Job } from './../../../../shared/job.model';
import { MessageService } from './../../../../shared/message.service';
import { Subscription } from 'rxjs/Subscription';
import { Client } from './../../../../shared/client.model';
import { ClientService } from './../../../../client.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
  styleUrls: ['./client-detail.component.css']
})
export class ClientDetailComponent implements OnInit {
  client: Client;
  loading = true;
  id: string;
  subscription: Subscription;
  showAddressBook = false;
  JobState: typeof JobState = JobState;

  constructor(private clientService: ClientService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {

  }

  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.id = params['id'];
        this.clientService.getClient(this.id)
          .then(client => this.client = client)
          .then(log => console.log(this.client))
          .then(loading => this.loading = false);
      }
      );
  }

  onNewJob() {
    this.router.navigate(['job/new'], { relativeTo: this.route });
  }

  onNewQuote() {
    this.router.navigate(['quote/new'], { relativeTo: this.route });
  }

  onEdit() {
    this.router.navigate(['clients/edit/' + this.client.id]);
  }

  onViewJobDetail(job: Job) {
    switch (job.jobState) {
      case JobState.Preproduction: {
        this.router.navigate(['preprod/' + job.id]);
        break;
      }
      case JobState.Quote: {
        this.router.navigate(['quotes/' + job.id]);
        break;
      }
      case JobState.Production: {
        this.router.navigate(['jobs/' + job.id]);
        break;
      }
      default: {
        break;
      }
    }
  }

  onDeleteJob(index: string) {
    this.clientService.deleteClient(this.id)
      .subscribe(
      (response: Response) => {
        console.log('job deleted');
      }
      );
  }

  onStop(onStop: Boolean) {

    if (onStop) {
      // TODO This works but need to make it look much nicer
      if (window.confirm('Are you sure you want to place this client on stop?')) {
        this.client.onStop = onStop;
        // TODO fix this
        this.client.contactId = this.client.contact.id;
        this.clientService.updateClientOnStop(this.client);
      }
    } else {
      // TODO This works but need to make it look much nicer
      if (window.confirm('Are you sure you want remove this client from stop?')) {
        this.client.onStop = onStop;
        // TODO fix this
        this.client.contactId = this.client.contact.id;
        this.clientService.updateClientOnStop(this.client);
      }
    }
  }

  onAddressBook() {
    this.showAddressBook = !this.showAddressBook;
  }
}
