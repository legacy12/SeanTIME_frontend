import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';

@Component({
  selector: 'app-client-list',
  templateUrl: './client-list.component.html',
  styleUrls: ['./client-list.component.css'],
})
export class ClientListComponent implements OnInit {
  clients: Client[];
  loading = true;
  subscription: Subscription;

  constructor(private clientService: ClientService,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {

    this.clientService.getClientList()
    .then(clients => this.clients = clients)
    .then(loading => this.loading = false);

  }

  onNewClient() {
    this.router.navigate(['clients/new']);
  }

  onSelectClient(index: string) {
    this.router.navigate([index], {relativeTo: this.route});
  }
}
