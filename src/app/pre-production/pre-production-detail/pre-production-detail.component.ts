import { PreProductionService } from './../../shared/pre-production.service';
import { PreProduction } from './../../shared/pre-production.model';
import { Quote } from './../../shared/quote.model';
import { JobService } from './../../shared/job.service';
import { ContactService } from './../../shared/contact.service';
import { Contact } from './../../shared/contact.model';
import { StaffService } from '../../shared/staff.service';
import { User } from '../../shared/staff.model';
import { MessageService } from './../../shared/message.service';
import { Job } from './../../shared/job.model';
import { Note } from './../../shared/note.model';
import { ClientService } from './../../client.service';
import { Client } from './../../shared/client.model';
import { Component, OnInit, Inject } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';
import { Response } from '@angular/http';
import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { QuoteService } from '../../shared/quote.service';

@Component({
  selector: 'app-pre-production-detail',
  templateUrl: './pre-production-detail.component.html',
  styleUrls: ['./pre-production-detail.component.css']
})
export class PreProductionDetailComponent implements OnInit {
  jobForm: FormGroup;
  preProdId: string; // job ID
  client: Client;
  id: string; // client ID
  editMode = false;
  preProd: PreProduction;
  butDisabled = true;
  loading = true;
  dueDate = null;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private preProductionService: PreProductionService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router,
    public dialog: MatDialog) {
  }


  ngOnInit() {
    this.route.params
      .subscribe(
      (params: Params) => {
        this.preProdId = params['id'];
        if (this.preProdId != null) {
          this.preProductionService.getPreProduction(this.preProdId)
            .then(quote => this.preProd = quote)
            .then(log => console.log(this.preProd))
            .then(client => this.client = this.preProd.client)
            .then(init => this.initForm())
            .then(loading => this.loading = false)
            .then(clientId => this.id = this.preProd.client.id);
        } else {
          this.loading = false;
          this.initForm();
        }
      }
      );
  }

  private initForm() {
    let quoteTitle = '';
    let quoteContact = '';
    let quoteDescription = '';
    let quoteStaff = '';
    let client = '';
    let friendlyId = 0;

    quoteTitle = this.preProd.title;
    quoteContact = this.preProd.contact.name;
    quoteDescription = this.preProd.description;
    quoteStaff = this.preProd.user.name;
    client = this.preProd.client.name;
    friendlyId = this.preProd.friendlyId;

    this.jobForm = new FormGroup({
      'title': new FormControl(quoteTitle, Validators.required),
      'contact': new FormControl(quoteContact, Validators.required),
      'description': new FormControl(quoteDescription, Validators.required),
      'staff': new FormControl(quoteStaff, Validators.required),
      'client': new FormControl(client, Validators.required),
      'friendlyId': new FormControl(friendlyId, Validators.required)
    });

    this.jobForm.get('staff').disable();
    this.jobForm.get('contact').disable();
    this.jobForm.get('client').disable();
  }

  onUpgrade() {
      // TODO This works but need to make it look much nicer
      if (window.confirm('Are you sure you want convert this quote to a job?')) {
        this.openDialog();
      }
  }

  onEdit() {
    console.log('tet');
    this.router.navigate(['preprod/edit/' + this.client.id + '/' + this.preProd.id]);
  }

  onCopy() {
    console.log('btb');
    this.router.navigate(['preprod/copy/' + this.client.id + '/' + this.preProd.id]);
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(UpgradeModalComponent, {
      width: '260px',
      data: { date: this.dueDate }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.dueDate = result;
      console.log(this.dueDate);
      if (this.dueDate != null) {
        this.preProd.milestones.due = this.dueDate;
        this.preProductionService.upgradePreProduction(this.preProd);
        this.onCancel();
      }
    });
  }

  onCancel() {
    this.router.navigate(['clients/' + this.id]);
  }

}

@Component({
  selector: 'app-dialog-overview',
  templateUrl: 'upgrade-modal.html',
})
export class UpgradeModalComponent {

  constructor(
    public dialogRef: MatDialogRef<UpgradeModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
