import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { User } from '../../shared/staff.model';
import { StaffService } from '../../shared/staff.service';
import { Supplier } from '../../shared/supplier.model';
import { ClientService } from './../../client.service';
import { Address } from './../../shared/address.model';
import { Client } from './../../shared/client.model';
import { Contact } from './../../shared/contact.model';
import { ContactService } from './../../shared/contact.service';
import { JobState } from './../../shared/jobstate.enum';
import { MessageService } from './../../shared/message.service';
import { Milestones } from './../../shared/milestones.model';
import { Note } from './../../shared/note.model';
import { PreItem } from './../../shared/pre-item.model';
import { PreProduction } from './../../shared/pre-production.model';
import { PreProductionService } from './../../shared/pre-production.service';
import { Quote } from './../../shared/quote.model';
import { SupplierService } from './../../shared/supplier.service';

@Component({
  selector: 'app-new-pre-production',
  templateUrl: './new-pre-production.component.html',
  styleUrls: ['./new-pre-production.component.css']
})
export class NewPreProductionComponent implements OnInit {
  client: Client;
  preProduction: PreProduction;
  staff: User;
  contact: Contact;
  id: string; // client ID
  preProductionId: string; // Quote ID
  editMode = false;
  preProductionForm: FormGroup;
  staffList: User[];
  contactList: Contact[];
  supplierList: Supplier[];
  loading = true;
  preItems: PreItem[];
  deliveryAddress: string;
  showAddressBook = false;
  activeItems = false;

  constructor(private clientService: ClientService,
    private staffService: StaffService,
    private contactService: ContactService,
    private supplierService: SupplierService,
    private preProductionService: PreProductionService,
    private route: ActivatedRoute,
    private messageService: MessageService,
    private router: Router) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.preProductionId = params['id'];
          this.id = params['cid'];
        }
      );

    if (this.preProductionId != null) {
      this.editMode = true;

      Promise.all([this.staffService.getStaffList()
        .then(staffList => this.staffList = staffList),
      this.contactService.getClientContactList()
        .then(contactList => this.contactList = contactList),
      this.clientService.getClient(this.id)
        .then(client => this.client = client),
      this.preProductionService.getPreProduction(this.preProductionId)
        .then(preProduction => this.preProduction = preProduction),
      this.supplierService.getSupplierList()
        .then(supplierList => this.supplierList = supplierList)])
        .then(log => console.log(this.contactList))
        .then(init => this.initForm())
        .then(loading => this.loading = false);
    } else {
      Promise.all([this.staffService.getStaffList()
        .then(staffList => this.staffList = staffList),
      this.contactService.getClientContactList()
        .then(contactList => this.contactList = contactList),
      this.clientService.getClient(this.id)
        .then(client => this.client = client),
      this.supplierService.getSupplierList()
        .then(supplierList => this.supplierList = supplierList)])
        .then(log => console.log(this.contactList))
        .then(init => this.initForm())
        .then(loading => this.loading = false);
    }
  }

  onSubmit() {

    if (this.client.quotes == null) {
      this.client.quotes = new Array<Quote>();
    }

    if (this.editMode) {
      this.preProduction.title = this.preProductionForm.value['title'];
      this.preProduction.contact = this.preProductionForm.value['contact'];
      this.preProduction.description = this.preProductionForm.value['description'];
      this.preProduction.user = this.preProductionForm.value['staff'];
      this.preProduction.preItems = this.preItems;

      this.preProductionService.updatePreProduction(this.preProduction)
        .then(cancel => this.onCancel());
    } else {
      this.contact = this.preProductionForm.value['contact'];
      this.staff = this.preProductionForm.value['staff'];
      const displayTime = new Date();
      const milestones = new Milestones(displayTime);
      console.log(this.preItems);
      const newPreProduction = new PreProduction(this.id,
        this.staff.id,
        this.contact.id,
        this.preProductionForm.value['title'],
        this.preProductionForm.value['description'],
        new Array<Note>(),
        milestones,
        this.preItems,
        JobState.Preproduction,
        this.deliveryAddress
      );

      console.log(newPreProduction);
      this.preProductionService.addNewPreProduction(newPreProduction)
        .then(cancel => this.onCancel());
    }

  }

  onCancel() {
    this.router.navigate(['clients/', this.id]);
  }

  private initForm() {
    let clientName;
    let QuoteTitle;
    let QuoteContact;
    let QuoteDescription;
    let QuoteStaff;
    let deliveryAddress;
    const itemProduct = '';
    const itemAmount = 0;
    const itemDescription = '';
    const itemSuppliers = '';
    const supplierContact = '';

    const preItems =
      new FormGroup({
        'itemDescription': new FormControl(itemDescription, Validators.required),
        'product': new FormControl(itemProduct, Validators.required),
        'amount': new FormControl(itemAmount, Validators.required)
      });

    if (this.editMode) {
      clientName = this.client.name;
      QuoteTitle = this.preProduction.title;
      QuoteContact = this.preProduction.contact;
      QuoteDescription = this.preProduction.description;
      QuoteStaff = this.preProduction.user.name;
      deliveryAddress = this.preProduction.deliveryAddress;
    }

    this.preProductionForm = new FormGroup({
      'title': new FormControl(QuoteTitle, Validators.required),
      'contact': new FormControl(QuoteContact, Validators.required),
      'description': new FormControl(QuoteDescription, Validators.required),
      'staff': new FormControl(QuoteStaff, Validators.required),
      'selectedAddress': new FormControl(null, Validators.required),
      'preItems': preItems,
      'items': new FormArray([
        new FormGroup({
          'supplier': new FormControl(null, Validators.required),
          'contact': new FormControl(null, Validators.required)
        })
      ], Validators.required)
    });

    console.log(this.preProductionForm);

    if (this.editMode) {
      const selectedStaff = this.staffList.find(staff => staff.id === this.preProduction.user.id);
      this.preProduction.user = selectedStaff;
      console.log(this.preProduction.user);
      this.preProductionForm.controls['staff'].setValue(this.preProduction.user, { onlySelf: true });

      const selectedContact = this.contactList.find(contact => contact.id === this.preProduction.contactId);
      this.preProduction.contact = selectedContact;
      console.log(this.preProduction.contact);
      this.preProductionForm.controls['contact'].setValue(this.preProduction.contact, { onlySelf: true });

      // TODO PreProduction needs Addresses added
      // const selectedAddress = this.client.addresses.find(address => address.formattedAddress === this.preProduction.deliveryAddress);
      // this.preProduction.deliveryAddress = selectedAddress;
      // console.log(this.preProduction.contact);
      // this.preProductionForm.controls['contact'].setValue(this.preProduction.contact, { onlySelf: true });

    }
  }

  onAddressComplete(address: Address) {
    // TODO onAddressComplete gets triggered too early
    if (address != null) {
      this.deliveryAddress = address.formattedAddress;
      console.log(this.deliveryAddress);
      this.client.addresses.push(address);
    }
  }

  viewAddressBook() {
    this.showAddressBook = !this.showAddressBook;
  }

  onAddPreItem(preItems: PreItem[]) {
    this.preItems = preItems;
    if (this.preItems.length > 0) {
      this.activeItems = true;
    } else {
      this.activeItems = false;
    }
  }
}
