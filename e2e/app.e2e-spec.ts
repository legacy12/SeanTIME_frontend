import { SeanTIMEPage } from './app.po';

describe('sean-time App', () => {
  let page: SeanTIMEPage;

  beforeEach(() => {
    page = new SeanTIMEPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
